<?php
session_start();
include("connection.php");
if(!isset($_SESSION['username'])){
     header("location: adminlogin.php");
 }
if ($_SESSION['role'] !='1' ){
    header("location: adminlogin.php");
}



?>
    <!DOCTYPE html>
    <html>
    <title>Admin Dashboard</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
    <link rel="stylesheet" href="w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <style>
        body,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "Raleway", sans-serif
        }
        
        .w3-row-padding img {
            margin-bottom: 12px
        }
        /* Set the width of the sidebar to 120px */
        
        .w3-sidebar {
            width: 120px;
            background: #222;
        }
        /* Add a left margin to the "page content" that matches the width of the sidebar (120px) */
        
        #main {
            margin-left: 120px
        }
        /* Remove margins from "page content" on small screens */
        
        @media only screen and (max-width: 600px) {
            #main {
                margin-left: 0
            }
        }

    </style>

    <body class="w3-black">

        <!-- Icon Bar (Sidebar - hidden on small screens) -->
        <nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center">
            <!-- Avatar image in top left corner -->
            <a href="index.php" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
                <i class="fa fa-home w3-xxlarge"></i>
                <p>HOME</p>
            </a>
            <a href="update_admin.php" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
                <i class="fa fa-user w3-xxlarge"></i>
                <p>YOUR PROFILE</p>
            </a>
            <a href="feedback.php" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
                <i class="fa fa-comments-o w3-xxlarge" aria-hidden="true"></i>
                <p>USERS' FEEDBACK</p>
            </a>
            <a href="manageusers.php" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
                <i class="fa fa-eye w3-xxlarge"></i>
                <p>MANAGE USERS</p>
            </a>
            <a href="logout.php" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
                <i class="fa fa-sign-out w3-xxlarge"></i>
                <p>LOG OUT</p>
            </a>
        </nav>

        <!-- Navbar on small screens (Hidden on medium and large screens) -->
        <div class="w3-top w3-hide-large w3-hide-medium" id="myNavbar">
            <div class="w3-bar w3-black w3-opacity w3-hover-opacity-off w3-center w3-small">
                <a href="index.php" class="w3-bar-item w3-button" style="width:25% !important">HOME  <i class="fa fa-home" style="font-size:22px"></i></a>
                <a href="update_admin.php" class="w3-bar-item w3-button" style="width:25% !important">YOUR PROFILE <i class="fa fa-user-secret" style="font-size:22px"></i></a>
                <a href="manageusers.php" class="w3-bar-item w3-button" style="width:25% !important">MANAGE USERS<i class="fa fa-address-card-o" style="font-size:22px"></i></a>
                <a href="logout.php" class="w3-bar-item w3-button" style="width:25% !important">LOG OUT<i class="fa fa-sign-out" style="font-size:22px"></i></a>
            </div>
        </div>

        <!-- Page Content -->
        <div class="w3-padding-large" id="main">
            <!-- Header/Home -->

            <header class="w3-container w3-padding-32 w3-center w3-black" id="home">
                <!--            <h1 class="w3-jumbo"><span class="w3-hide-small">Powered by</span> Strathmore University.</h1>-->
                <?php echo "<h5 style='text-align: center; margin-top: 50px; '>Welcome Administrator ".$_SESSION['username']. ".<h5>";?>
                <img src="images/su_png.png" alt="STRATHMORE UNIVERSITY" class="w3-image" width="992" height="1108">
            </header>



            <footer class="w3-content w3-padding-64 w3-text-grey w3-xlarge">
                <p class="w3-large" style="text-align:center">Powered by <a href="https://www.strathmore.edu" target="_blank" class="w3-hover-text-green">Strathmore University</a></p>
                <!-- End footer -->
            </footer>

            <!-- END PAGE CONTENT -->
        </div>

    </body>

    </html>
