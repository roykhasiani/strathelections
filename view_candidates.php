<?php  
session_start();
include("connection.php");

if (!$_SESSION['auth']){
    header("location: login.php");
}

if (isset($_POST["update"])){
    $updateQuery="UPDATE students SET studentID = '$_POST[studentID]'  WHERE studentID =' $_POST[hidden]';";
   
    
$result =mysqli_multi_query($conn, $updateQuery);
    
    if ($result) {
         $updateQuery2= "UPDATE candidates SET Fname='$_POST[Fname]', Lname='$_POST[Lname]', positions_positionID ='$_POST[positionID]' WHERE students_studentID =' $_POST[hidden]'";
        mysqli_multi_query($conn, $updateQuery2);
            header( "refresh:1; url=view_candidates.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Data Updated Successfully !!!</b></h3>
  </div>";
    
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $updateQuery . "<br>" . mysqli_error($conn)."<p></div>";
    }
    
}

if (isset($_POST["delete"])){
// $deleteQuery = "DELETE FROM students WHERE studentID =' $_POST[hidden]'";
    $deleteQuery = "DELETE FROM candidates WHERE students_studentID =' $_POST[hidden]';";
//    $deleteQuery .= "DELETE FROM candidatevotes WHERE candidates_students_studentID = ' $_POST[hidden]'";
    
$result =mysqli_multi_query($conn, $deleteQuery);
    
    if ($result) {
        $deleteQuery2 = "DELETE FROM candidatevotes WHERE candidates_students_studentID = ' $_POST[hidden]'";
          mysqli_query($conn, $deleteQuery2);
        //to update the student table
        $deleteQuery3="UPDATE `students` SET `role_roleID` = '3' WHERE `students`.`studentID` = '$_POST[hidden]' AND `students`.`role_roleID` = 2";
//        $deleteQuery3="UPDATE students SET role_roleID = 3 WHERE studentID = '$_POST[hidden]'";
      
        mysqli_query($conn, $deleteQuery3);
         header( "refresh:1; url=view_candidates.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Data Deleted Successfully !!!</b></h3>
  </div>";
       
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $deleteQuery . "<br>" . mysqli_error($conn)."<p></div>";
    }
    
}
 ?>




<!DOCTYPE html>
<html>
<title>Candidates</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="w3mobile.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<script src="w3.js"></script>

<style>
    html,
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", sans-serif
    }
    
    .search {
        position: relative;
        color: #aaa;
        font-size: 16px;
    }
    
    .search input {
        text-indent: 32px;
    }
    
    .search .fa-search {
        position: absolute;
        top: 10px;
        left: 10px;
    }

</style>

<body class="w3-light-grey">
    <div class="w3-bar w3-black">
        <a href="admin.php" class="w3-bar-item w3-button" style="width:20%;"><b>Admin Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
        <a href="manageusers.php" class="w3-bar-item w3-button" style="width:20%"><b>Add Users <i class="fa fa-user-plus" style="font-size:19px"></i></b></a>
        <a href="view_students.php" class="w3-bar-item w3-button" style="width:20%"><b>Student List  <i class="fa fa-address-card-o" style="font-size:22px"></i></b></a>
        <a href="update_bridge.php" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username'];?> Logged In  <i class="fa fa-user-secret" style="font-size:22px"></i> </b></a>
        <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
    </div>

    <div class="w3-container w3-sand w3-responsive" style="width:100%"><br>
        <h1 style="text-align:center"><b>REGISTERED CANDIDATES FOR THIS ELECTION</b></h1><br>
        <div class="search">
            <span class="fa fa-search"></span>
            <input class="w3-input w3-border w3-padding" oninput="w3.filterHTML('#id01', '.item', this.value)" placeholder="Search for Candidate..">
        </div>


        <p>
        </p>

        <table id="id01" class="w3-table-all w3-hoverable w3-sand w3-card-4" style="width:100%;">
            <tr class="w3-brown">
                <th>Student ID Number</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Position ID</th>
                <th>Action 1</th>
                <th>Action 2</th>
                <th></th>
            </tr>


            <?php
// define how many results you want per page
$results_per_page = 10;
// find out the number of results stored in database
 $sql = "SELECT students_studentID, Fname, Lname, positions_positionID FROM candidates ORDER BY positions_positionID ASC";
$result = mysqli_query($conn, $sql);
$number_of_results = mysqli_num_rows($result);
// determine number of total pages available
$number_of_pages = ceil($number_of_results/$results_per_page);
// determine which page number visitor is currently on
if (!isset($_GET['page'])) {
  $page = 1;
} else {
  $page = $_GET['page'];
}
// determine the sql LIMIT starting number for the results on the displaying page
$this_page_first_result = ($page-1)*$results_per_page;
// retrieve selected results from database and display them on page
$sql='SELECT * FROM candidates LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_array($result)) {
  echo "<form action='view_students.php' method='post'>";
       
                echo "<form action='view_candidates.php' method='post'>";
       
                echo "<tr class=' w3-sand w3-hoverable item'>";
                    echo"<td class='w3-text-brown'><b> "  ."<input type = text name=studentID value=" .$row["students_studentID"].
                    " </b></td>";
                    echo "<td class='w3-text-brown'><b> " ."<input type = text name=Fname value=" . $row["Fname"]." </b>
                    </td>";
                    echo "<td class='w3-text-brown'><b> " ."<input type = text name=Lname value=" .$row["Lname"]. " </b>
                    </td>";
                    echo "<td class='w3-text-brown'><b> " ."<input type = text name=positionID value=" .$row["positions_positionID"]. " </b>
                    </td>";
                    echo "<td class='w3-text-brown'>" . "<input type=submit name=update value=update" . " </td>";
                    echo "<td class='w3-text-brown'>" . "<input type=submit name=delete value=delete" . " </td>";
                    echo"<td> "  ."<input type = hidden name=hidden value=" .$row["students_studentID"]. " </td>";
                    echo "</tr>";
                    echo "</form>";
}
            
            
?>
        </table>
    </div>
    <!-- Pagination -->
    <div class="w3-center w3-padding-32">


        <?php
// display the links to the pages
for ($page=1;$page<=$number_of_pages;$page++) {
    
  echo '<div class="w3-bar"><a href="view_candidates.php?page=' . $page . '" class="w3-bar-item w3-black w3-button">' . $page . '</a></div> ';
}
?>

    </div>


    <footer class="w3-container w3-padding-32 w3-dark-grey">
        <div class="w3-row-padding">
            <div class="w3-third">
                <h3>FROM OUR OWN...</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/roy.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Roy Murwa Khasiani, a proud Stratizen ―</span><br>
                        <span>“We vote as one, for those who will keep us all in oneness, for the sake of achieving one vision, as one people.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>FAMOUS QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/alincoln.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Abraham Lincoln ―</span><br>
                        <span>“Elections belong to the people. It's their decision. If they decide to turn their back on the fire and burn their behinds, then they will just have to sit on their blisters.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>MORE QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/larrysabato.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Larry J. Sabato ―</span><br>
                        <span>“Every election is determined by the people who show up.” 
</span>
                    </li>
                </ul>
            </div>


            <div class="w3-black w3-center w3-padding-24">Powered by <a href="http://www.strathmore.edu" title="STRATHMORE UNIVERSITY" target="_blank" class="w3-hover-opacity">STRATHMORE UNIVERSITY</a></div>

            <!-- End page content -->
        </div>
    </footer>



</body>

</html>
