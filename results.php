<?php  
session_start();
include("connection.php");
// if (!$_SESSION['auth']){
//     header("location: login.php");
// }

 if(!isset($_SESSION['username'])){
     header("location: login.php");
 }
 
 if(isset($_POST["submit"])){
    $studentID = $_SESSION["username"];
    $feedback = $_POST["feedback"];
    $sql = "INSERT INTO feedback(students_studentID, message, sent_time) VALUES ('$studentID', '$feedback', NOW())";
    $result = mysqli_query($conn, $sql);
    if ($result) {
         header( "refresh:1; url=results.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>SUCCESSFULLY SENT YOUR FEEDBACK. THANK YOU!!!</b></h3>
  </div>";
       
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $sql . "<br>" . mysqli_error($conn)."<p></div>";
    }

}


 ?>



<!DOCTYPE html>
<html>
<title>Results</title>

<!--    Refresh page with new values in real time-->
<meta http-equiv="refresh" content="100">


<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<style>
    html,
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", sans-serif
    }
    
    #topcorner {
        margin-left: 700;
    }
    /*
 a:visited { color: grey; }*/

</style>

<body class="w3-light-grey">
    <div class="w3-bar w3-black">
        <a href="index.php" class="w3-bar-item w3-button" style="width:20%;"><b>Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
        <a href="vote.php" class="w3-bar-item w3-button" style="width:20%"><b>Vote <i class="fa fa-paper-plane-o" style="font-size:19px"></i></b></a>
        <a href="charts.php" class="w3-bar-item w3-button" style="width:20%"><b>Live Reports  <i class="fa fa-pie-chart" style="font-size:22px"></i> </b></a>
        <a href="update_bridge.php" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username']?> Logged In  <i class="fa fa-user-circle-o" style="font-size:22px"></i></b></a>
        <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
    </div>

    <div class="w3-container">
        <h2 style="text-align:center">STRATHMORE ELECTIONS RESULTS</h2>

        <div class="w3-round-large w3-brown w3-hide-small" style="float: right;width: 180px; height: 60px; margin-left: 10px;">
            <span> <?php
                            $notvoted = "SELECT vote_statusID FROM student_vote_status WHERE vote_statusID = 1";
                            $notvoted_result = mysqli_query($conn, $notvoted);
                            $registered_notvoted = mysqli_num_rows($notvoted_result);
                            echo "<p style='margin-left:10px;'>Total Pending Votes: ". $registered_notvoted. " </p>";
                        ?>
                        </span>
        </div>

        <div class="w3-round-large w3-brown w3-hide-small" style="float: right;width: 150px; height: 60px; margin-left: 10px;">
            <span> <?php
                            $voted = "SELECT vote_statusID FROM student_vote_status WHERE vote_statusID = 2";
                            $voted_result = mysqli_query($conn, $voted);
                            $registered_voted = mysqli_num_rows($voted_result);
                            echo "<p style='margin-left:10px;'>Total Votes Cast: ". $registered_voted. " </p>";
                        ?>
                        </span>
        </div>

        <div class="w3-round-large w3-brown w3-hide-small" style="float: right;width: 165px; height: 60px; margin-left: 10px;">
            <span> <?php
                            $voters = "SELECT studentID FROM student_vote_status";
                            $voters_result = mysqli_query($conn, $voters);
                            $registered_voters = mysqli_num_rows($voters_result);
                            echo "<p style='margin-left:10px;'>Registered Voters: ". $registered_voters. " </p>";
                        ?>
                        </span>
        </div>




        <!--    Display the current time-->
        <div class="w3-round-large w3-brown" style="float: right; width: 90px; height: 60px;">
            <p style="margin-left:10px;" id="clock"></p>

        </div>
        <h3>Chairperson Standings</h3>
        <table class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Votes</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 1 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand w3-hoverable">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:40px;height:35px"/>';?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Fname"];?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Lname"]; ?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["votes"]; ?>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3>Vice Chairperson Standings</h3>
        <table class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Votes</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 2 ORDER BY candidatevotes.votes  DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:40px;height:35px"/>';?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Fname"];?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Lname"]; ?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["votes"]; ?>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>
        <h3>Public Relations Representative Standings</h3>
        <table class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Votes</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 3 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:40px;height:35px"/>';?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Fname"];?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Lname"]; ?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["votes"]; ?>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>


        <h3>Sports Representative Standings</h3>
        <table class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Votes</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 4 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:40px;height:35px"/>';?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Fname"];?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Lname"]; ?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["votes"]; ?>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3>Academic Representative Standings</h3>
        <table class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Votes</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 5 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:40px;height:35px"/>';?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Fname"];?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Lname"]; ?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["votes"]; ?>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3>Financial Representative Standings</h3>
        <table class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Votes</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 6 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:40px;height:35px"/>';?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Fname"];?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Lname"]; ?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["votes"]; ?>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3>Secretary Standings</h3>
        <table class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Votes</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 7 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:40px;height:35px"/>';?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Fname"];?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["Lname"]; ?>
                </td>
                <td class="w3-text-brown">
                    <?php echo $row["votes"]; ?>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>
    </div>


    <!-- Contact Section -->
    <div class="w3-container w3-padding-large w3-grey">
        <h4 id="contact">
            <center><b>Please Give Us Your Feedback On This System</b></center>
        </h4>

        <hr class="w3-opacity">
        <form action="" method="post">
            <div class="w3-section">
                <p>
                    <label><b>Your Feedback</b></label>
                    <textarea class="w3-input w3-border" style="resize:none" name="feedback" required></textarea>
                </p>
            </div>
            <button type="submit" name="submit" class="w3-button w3-black w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Send Message</button>
        </form>

        <hr class="w3-opacity">
        <h4 id="contact">
            <center><b>Contact Us</b></center>
        </h4>
        <div class="w3-row-padding w3-center w3-padding-16" style="margin:0 -16px">
            <div class="w3-third w3-dark-grey">
                <p><i class="fa fa-envelope w3-xxlarge w3-text-light-grey"></i></p>
                <p>strathelections@gmail.com</p>
            </div>
            <div class="w3-third w3-teal">
                <p><i class="fa fa-map-marker w3-xxlarge w3-text-light-grey"></i></p>
                <p>Nairobi, Kenya</p>
            </div>
            <div class="w3-third w3-dark-grey">
                <p><i class="fa fa-phone w3-xxlarge w3-text-light-grey"></i></p>
                <p>0790434029</p>
            </div>
        </div>
    </div>


    <footer class="w3-container w3-padding-32 w3-dark-grey">
        <div class="w3-row-padding">
            <div class="w3-third">
                <h3>FROM OUR OWN...</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/roy.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Roy Murwa Khasiani, a proud Stratizen ―</span><br>
                        <span>“We vote as one, for those who will keep us all in oneness, for the sake of achieving one vision, as one people.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>FAMOUS QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/alincoln.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Abraham Lincoln ―</span><br>
                        <span>“Elections belong to the people. It's their decision. If they decide to turn their back on the fire and burn their behinds, then they will just have to sit on their blisters.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>MORE QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/larrysabato.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Larry J. Sabato ―</span><br>
                        <span>“Every election is determined by the people who show up.” 
</span>
                    </li>
                </ul>
            </div>


            <div class="w3-black w3-center w3-padding-24">Powered by <a href="http://www.strathmore.edu" title="STRATHMORE UNIVERSITY" target="_blank" class="w3-hover-opacity">STRATHMORE UNIVERSITY</a></div>

            <!-- End page content -->
        </div>
    </footer>



    <script>
        (function() {

            var clockElement = document.getElementById("clock");

            function updateClock(clock) {
                clock.innerHTML = new Date().toLocaleTimeString();
            }

            setInterval(function() {
                updateClock(clockElement);
            }, 1000);

        }());

    </script>

</body>


</html>
