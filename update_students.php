<?php  
session_start();
include("connection.php");



if (!isset($_SESSION['auth'])){
    header("location: login.php");
}


 $username = $_SESSION['username'];
            $select_manifesto="SELECT manifesto_Content, has_manifesto FROM candidates WHERE students_studentID = ' $username'";
            $s_manifesto_res=mysqli_query($conn, $select_manifesto);
                        if ($s_manifesto_res){
                if(mysqli_num_rows($s_manifesto_res) > 0)  
                          {
                while($row = mysqli_fetch_array($s_manifesto_res))  
                               { 
                $value_returned = $row["has_manifesto"];
                $manifesto_Content = $row["manifesto_Content"];
            }}}
            
if (isset($_POST["update"])){
     $password = mysqli_real_escape_string($conn, $_POST["password"]);
    $hash= password_hash($password, PASSWORD_BCRYPT, array('cost'=>11));
    if ($_SESSION['role']==2){
    if($value_returned == 1){
        if(isset($_POST['manifesto'])){
        $manifesto =  $_POST["manifesto"];
        $updateQuery1 = "UPDATE students SET password='$hash' WHERE studentID =' $_POST[hidden]'";
        $insertQuery = "UPDATE candidates SET `manifesto_Content`= '$manifesto' WHERE students_studentID =' $_POST[hidden]'";
        $update_has_manifesto = "UPDATE candidates SET `has_manifesto`= 2 WHERE students_studentID =' $_POST[hidden]'";
        $resultupdate =mysqli_query($conn, $updateQuery1);
        $result2 =mysqli_query($conn, $insertQuery);
        $result3 =mysqli_query($conn, $update_has_manifesto);
        if ($resultupdate && $result2 && $result3) {
        session_destroy();
        header( "refresh:1; url=login.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Data Updated Successfully !!!</b></h3>
  </div>";
        
    } else {
        
        echo "<div class='w3-panel w3-red'><p>Error: " . $update_has_manifesto . "<br>" . mysqli_error($conn)."<p></div>";
    }}
    }else{
    $updateQuery2 = "UPDATE students SET password='$hash' WHERE studentID ='$_POST[hidden]'";
    $result =mysqli_query($conn, $updateQuery2);
   
    if ($result) {
        session_destroy();
        header( "refresh:1; url=login.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Data Updated Successfully !!!</b></h3>
  </div>";
        
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $updateQuery . "<br>" . mysqli_error($conn)."<p></div>";
    }
    }
}
}
 ?>



<!DOCTYPE html>
<html>
<title>Update Student</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<script src="w3.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea'
    });

</script>

<style>
    html,
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", sans-serif
    }
    
    .search {
        position: relative;
        color: #aaa;
        font-size: 16px;
    }
    
    .search input {
        text-indent: 32px;
    }
    
    .search .fa-search {
        position: absolute;
        top: 10px;
        left: 10px;
    }

</style>

<body class="w3-light-grey">
    <div class="w3-bar w3-black">
        <a href="index.php" class="w3-bar-item w3-button" style="width:20%;"><b>Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
        <a href="results.php" class="w3-bar-item w3-button" style="width:20%"><b>Results <i class="fa fa-envelope-o" style="font-size:19px"></i></b></a>
        <a href="charts.php" class="w3-bar-item w3-button" style="width:20%"><b>Live Reports  <i class="fa fa-pie-chart" style="font-size:22px"></i> </b></a>
        <a href="update_bridge.php" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username']?> Logged In  <i class="fa fa-user-circle-o" style="font-size:22px"></i></b></a>
        <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
    </div>


    <div class="w3-container w3-sand"><br>
        <h1 style="text-align:center"><b>YOUR DETAILS</b></h1><br>
        <h2 style="text-align:center"><b>YOU CAN ONLY CHANGE YOUR PASSWORD</b></h2><br>
        <h4 style="text-align:center">(You have to log in again once you change your password)</h4>
        <!--
<div class="search">
    <span class="fa fa-search"></span>
    <input class="w3-input w3-border w3-padding" oninput="w3.filterHTML('#id01', '.item', this.value)" placeholder="Search for Student..">
</div>
<p></p>
-->
        <table id="id01" class="w3-table-all w3-hoverable w3-sand w3-card-4 w3-responsive">
            <tr class="w3-brown">
                <th>Your New Password</th>
                <th>Action</th>
                <th></th>
            </tr>
            <?php  
           

            
             $sql = "SELECT studentID ,password FROM students WHERE studentID = $username";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          
                echo "<form action='update_students.php' method='post' id='usrform' >";
       
                echo "<tr class='w3-sand w3-hoverable item'>";
                    echo "<td class='w3-text-brown'><b> " ."<input type = password name=password required pattern='(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}' title='Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters' </b>
                    </td>";
                     if($_SESSION['role']==2){
                        
                        if ($value_returned==2){
                        echo "<h3>You Have Already Submitted Your Manifesto</h3>";
                    }else{ 
                        echo "<h3>INPUT YOUR MANIFESTO BELOW. YOU CANNOT EDIT IT AGAIN. AVOID MISTAKES!!!<h3>";
                        echo "<textarea rows='4' cols='50' name='manifesto'></textarea>";
                    }}
                    echo "<td class='w3-text-brown'>" . "<input type=submit name=update value=update" . " </td>";
                    echo"<td> "  ."<input type = hidden name=hidden value=" .$row["studentID"]. " </td>";
                    echo "</tr>";
                    
                    echo "</form>";
                   

                               }  
                          }  
                          ?>
        </table>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
    <footer class="w3-container w3-padding-32 w3-dark-grey">
        <div class="w3-row-padding">
            <div class="w3-third">
                <h3>FROM OUR OWN...</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/roy.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Roy Murwa Khasiani, a proud Stratizen ―</span><br>
                        <span>“We vote as one, for those who will keep us all in oneness, for the sake of achieving one vision, as one people.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>FAMOUS QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/alincoln.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Abraham Lincoln ―</span><br>
                        <span>“Elections belong to the people. It's their decision. If they decide to turn their back on the fire and burn their behinds, then they will just have to sit on their blisters.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>MORE QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/larrysabato.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Larry J. Sabato ―</span><br>
                        <span>“Every election is determined by the people who show up.” 
</span>
                    </li>
                </ul>
            </div>


            <div class="w3-black w3-center w3-padding-24">Powered by <a href="http://www.strathmore.edu" title="STRATHMORE UNIVERSITY" target="_blank" class="w3-hover-opacity">STRATHMORE UNIVERSITY</a></div>

            <!-- End page content -->
        </div>
    </footer>




</body>

</html>
