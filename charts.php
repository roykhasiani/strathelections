<?php
session_start();
include("connection.php");

// if (!$_SESSION['auth']){
//     header("location: login.php");
// }

 if(!isset($_SESSION['username'])){
     header("location: login.php");
 }
?>


    <html>

    <head>
        <title>Reports</title>

        <!--    Refresh page with new values in real time-->
        <meta http-equiv="refresh" content="60">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="w3.css">
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
        <style>
            html,
            body,
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-family: "Raleway"
            }
            
            div.gallery {
                /*            border: 1px solid #ccc;*/
            }
            
            div.gallery:hover {
                /*            border: 1px solid #777;*/
            }
            
            div.gallery img {
                width: 100%;
                height: auto;
            }
            
            div.desc {
                padding: 15px;
                text-align: center;
            }
            
            * {
                box-sizing: border-box;
            }
            
            .responsive {
                padding: 0 6px;
                float: left;
                width: 24.99999%;
            }
            
            @media only screen and (max-width: 1600px) {
                .responsive {
                    width: 49.99999%;
                    margin: 6px 0;
                }
            }
            
            @media only screen and (max-width: 500px) {
                .responsive {
                    width: 100%;
                }
            }
            
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }

        </style>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            // Load google charts
            google.charts.load('current', {
                'packages': ['corechart']
            });



            google.charts.setOnLoadCallback(drawChart1);
            google.charts.setOnLoadCallback(drawChart2);
            google.charts.setOnLoadCallback(drawChart3);
            google.charts.setOnLoadCallback(drawChart4);
            google.charts.setOnLoadCallback(drawChart5);
            google.charts.setOnLoadCallback(drawChart6);
            google.charts.setOnLoadCallback(drawChart7);
            //
            //
            //            setInterval(function() {
            //                google.charts.setOnLoadCallback(drawChart1);
            //            }, 1000);

            // Draw the chart and set the chart values
            function drawChart1() {
                var data = new google.visualization.arrayToDataTable([
                    ['Candidate Name', 'Votes'],
                    <?php

                        include("connection.php");


                                     $sql = "SELECT candidates.positions_positionID, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 1 ORDER BY candidatevotes.votes DESC";  

                        $result = mysqli_query($conn, $sql);


                        if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
                        if(mysqli_num_rows($result) > 0)  
                                                  {  
                                                       while($row = mysqli_fetch_array($result))  
                                                       {  


                ?>

                    ['<?php echo $row["Fname"]." "; echo $row["Lname"];?>', <?php echo $row["votes"];?>],
                    <?php
                                                       }
                        }
                    ?>
                ]);


                // Optional; add a title and set the width and height of the chart
                var options = {
                    'title': 'Chairperson Contestants',
                    'width': 550, // 
                    'height': 400,
                    'fontName': 'Raleway',
                    'fontSize': 14,
                    'bold': true,
                    'is3D': true
                };

                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart1'));


                chart.draw(data, options);


            }

            function drawChart2() {
                var data = new google.visualization.arrayToDataTable([
                    ['Candidate Name', 'Votes'],
                    <?php
                        include("connection.php");
// if (!$_SESSION['auth']){ // header("location: candidates.php"); // } // // echo "Welcome User ". $_SESSION['username'].".";

                                     $sql = "SELECT candidates.positions_positionID, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 2 ORDER BY candidatevotes.votes DESC";  

                        $result = mysqli_query($conn, $sql);


                        if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
                        if(mysqli_num_rows($result) > 0)  
                                                  {  
                                                       while($row = mysqli_fetch_array($result))  
                                                       {  


                ?>

                    ['<?php echo $row["Fname"]." "; echo $row["Lname"];?>', <?php echo $row["votes"];?>],
                    <?php
                                                       }
                        }
                    ?>
                ]);


                // Optional; add a title and set the width and height of the chart
                var options = {
                    'title': 'Vice Chairperson Contestants',
                    'width': 550,
                    'height': 400,
                    'fontName': 'Raleway',
                    'fontSize': 14,
                    'bold': true,
                    'is3D': true
                };

                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

                chart.draw(data, options);


            }


            function drawChart3() {
                var data = new google.visualization.arrayToDataTable([
                    ['Candidate Name', 'Votes'],
                    <?php
                        include("connection.php");
// if (!$_SESSION['auth']){ // header("location: candidates.php"); // } // // echo "Welcome User ". $_SESSION['username'].".";

                                     $sql = "SELECT candidates.positions_positionID, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 3 ORDER BY candidatevotes.votes DESC";  

                        $result = mysqli_query($conn, $sql);


                        if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
                        if(mysqli_num_rows($result) > 0)  
                                                  {  
                                                       while($row = mysqli_fetch_array($result))  
                                                       {  


                ?>

                    ['<?php echo $row["Fname"]." "; echo $row["Lname"];?>', <?php echo $row["votes"];?>],
                    <?php
                                                       }
                        }
                    ?>
                ]);


                // Optional; add a title and set the width and height of the chart
                var options = {
                    'title': 'Public Relations Representative Contestants',
                    'width': 550,
                    'height': 400,
                    'fontName': 'Raleway',
                    'fontSize': 14,
                    'bold': true,
                    'is3D': true
                };

                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

                chart.draw(data, options);


            }


            function drawChart4() {
                var data = new google.visualization.arrayToDataTable([
                    ['Candidate Name', 'Votes'],
                    <?php
                        include("connection.php");
// if (!$_SESSION['auth']){ // header("location: candidates.php"); // } // // echo "Welcome User ". $_SESSION['username'].".";

                                     $sql = "SELECT candidates.positions_positionID, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 4 ORDER BY candidatevotes.votes DESC";  

                        $result = mysqli_query($conn, $sql);


                        if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
                        if(mysqli_num_rows($result) > 0)  
                                                  {  
                                                       while($row = mysqli_fetch_array($result))  
                                                       {  


                ?>

                    ['<?php echo $row["Fname"]." "; echo $row["Lname"];?>', <?php echo $row["votes"];?>],
                    <?php
                                                       }
                        }
                    ?>
                ]);


                // Optional; add a title and set the width and height of the chart
                var options = {
                    'title': 'Sports Representative Contestants',
                    'width': 550,
                    'height': 400,
                    'fontName': 'Raleway',
                    'fontSize': 14,
                    'bold': true,
                    'is3D': true
                };

                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart4'));

                chart.draw(data, options);


            }



            function drawChart5() {
                var data = new google.visualization.arrayToDataTable([
                    ['Candidate Name', 'Votes'],
                    <?php
                        include("connection.php");
// if (!$_SESSION['auth']){ // header("location: candidates.php"); // } // // echo "Welcome User ". $_SESSION['username'].".";

                                     $sql = "SELECT candidates.positions_positionID, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 5 ORDER BY candidatevotes.votes DESC";  

                        $result = mysqli_query($conn, $sql);


                        if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
                        if(mysqli_num_rows($result) > 0)  
                                                  {  
                                                       while($row = mysqli_fetch_array($result))  
                                                       {  


                ?>

                    ['<?php echo $row["Fname"]." "; echo $row["Lname"];?>', <?php echo $row["votes"];?>],
                    <?php
                                                       }
                        }
                    ?>
                ]);


                // Optional; add a title and set the width and height of the chart
                var options = {
                    'title': 'Academic Representative Contestants',
                    'width': 550,
                    'height': 400,
                    'fontName': 'Raleway',
                    'fontSize': 14,
                    'bold': true,
                    'is3D': true
                };

                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart5'));

                chart.draw(data, options);


            }


            function drawChart6() {
                var data = new google.visualization.arrayToDataTable([
                    ['Candidate Name', 'Votes'],
                    <?php
                        include("connection.php");

                                     $sql = "SELECT candidates.positions_positionID, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 6 ORDER BY candidatevotes.votes DESC";  

                        $result = mysqli_query($conn, $sql);


                        if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
                        if(mysqli_num_rows($result) > 0)  
                                                  {  
                                                       while($row = mysqli_fetch_array($result))  
                                                       {  


                ?>

                    ['<?php echo $row["Fname"]." "; echo $row["Lname"];?>', <?php echo $row["votes"];?>],
                    <?php
                                                       }
                        }
                    ?>
                ]);


                // Optional; add a title and set the width and height of the chart
                var options = {
                    'title': 'Financial Representative Contestants',
                    'width': 550,
                    'height': 400,
                    'fontName': 'Raleway',
                    'fontSize': 14,
                    'bold': true,
                    'is3D': true
                };

                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart6'));

                chart.draw(data, options);


            }


            function drawChart7() {
                var data = new google.visualization.arrayToDataTable([
                    ['Candidate Name', 'Votes'],
                    <?php
                        include("connection.php");
// if (!$_SESSION['auth']){ // header("location: candidates.php"); // } // // echo "Welcome User ". $_SESSION['username'].".";

                                     $sql = "SELECT candidates.positions_positionID, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 7 ORDER BY candidatevotes.votes DESC";  

                        $result = mysqli_query($conn, $sql);


                        if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
                        if(mysqli_num_rows($result) > 0)  
                                                  {  
                                                       while($row = mysqli_fetch_array($result))  
                                                       {  


                ?>

                    ['<?php echo $row["Fname"]." "; echo $row["Lname"];?>', <?php echo $row["votes"];?>],
                    <?php
                                                       }
                        }
                    ?>
                ]);


                // Optional; add a title and set the width and height of the chart
                var options = {
                    'title': 'Secretary Contestants',
                    'width': 550,
                    'height': 400,
                    'fontName': 'Raleway',
                    'fontSize': 14,
                    'bold': true,
                    'is3D': true
                };

                // Display the chart inside the <div> element with id="piechart"
                var chart = new google.visualization.PieChart(document.getElementById('piechart7'));

                chart.draw(data, options);


            }

        </script>


    </head>

    <body class="w3-light-grey" style="font-family: Raleway;">


        <div class="w3-bar w3-black">
            <a href="index.php" class="w3-bar-item w3-button" style="width:20%;"><b>Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
            <a href="#" class="w3-bar-item w3-button" style="width:20%" onclick="close_voting();"><b>Vote <i class="fa fa-paper-plane-o" style="font-size:19px"></i></b></a>
            <a href="results.php" class="w3-bar-item w3-button" style="width:20%"><b>Back To Results  <i class="fa fa-envelope-open-o" style="font-size:22px"></i> </b></a>
            <a href="update_bridge.php" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username'];?> Logged In  <i class="fa fa-user-circle-o" style="font-size:22px"></i></b></a>
            <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
        </div>

        <div class="w3-container w3-sand">
            <h2 style="text-align:center">LIVE SUMMARY OF THE RESULTS</h2>
            <button class="printbtn" onclick="window.print()">Print Page</button>
            <div class="w3-round-large w3-brown" style="float: right; width: 90px; height: 60px;">
                <p style="margin-left:10px;" id="clock"></p>

            </div>

        </div>
        <div class="responsive w3-card-4 w3-sand">
            <div id="piechart1" class="gallery">
            </div>
        </div>


        <div class="responsive w3-card-4 w3-sand">
            <div id="piechart2" class="gallery">
            </div>
        </div>

        <div class="responsive w3-card-4 w3-sand">
            <div id="piechart3" class="gallery">
            </div>
        </div>

        <div class="responsive w3-card-4 w3-sand">
            <div id="piechart4" class="gallery">
            </div>
        </div>

        <div class="responsive w3-card-4 w3-sand">
            <div id="piechart5" class="gallery">
            </div>
        </div>

        <div class="responsive w3-card-4 w3-sand">
            <div id="piechart6" class="gallery">
            </div>
        </div>

        <div class="responsive w3-card-4 w3-sand">
            <div id="piechart7" class="gallery">
            </div>
        </div>

        <div class="clearfix"></div>


        <script>
            (function() {

                var clockElement = document.getElementById("clock");

                function updateClock(clock) {
                    clock.innerHTML = new Date().toLocaleTimeString();
                }

                setInterval(function() {
                    updateClock(clockElement);
                }, 1000);

            }());

            function close_voting() {
                var d = new Date();
                if (d.getHours() >= 6 && d.getHours() <= 21) {
                    // alert("VOTING PERIOD HAS CLOSED");
                    window.location = "vote.php";
                } else {
                    alert("VOTING PERIOD HAS CLOSED");

                }
            }

        </script>


    </body>

    </html>
