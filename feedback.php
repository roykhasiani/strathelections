<?php
    session_start();
    include "connection.php";
?>


    <!DOCTYPE html>
    <html>
    <title>Feedback</title>
    <meta charset="UTF-8">

    <meta http-equiv="refresh" content="30">

    <title>Update Student</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="w3.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="w3.js"></script>

    <style>
        body,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "Raleway", sans-serif
        }
        
        .example-obtuse {
            position: relative;
            padding: 15px 30px;
            margin: 0;
            color: #000;
            background: #f3961c;
            /* default background for browsers without gradient support */
            /* css3 */
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#f9d835), to(#f3961c));
            background: -moz-linear-gradient(#f9d835, #f3961c);
            background: -o-linear-gradient(#f9d835, #f3961c);
            background: linear-gradient(#f9d835, #f3961c);
            /* Using longhand to avoid inconsistencies between Safari 4 and Chrome 4 */
            -webkit-border-top-left-radius: 25px 50px;
            -webkit-border-top-right-radius: 25px 50px;
            -webkit-border-bottom-right-radius: 25px 50px;
            -webkit-border-bottom-left-radius: 25px 50px;
            -moz-border-radius: 25px / 50px;
            border-radius: 25px / 50px;
        }
        /* display of quote author (alternatively use a class on the element following the blockquote) */
        
        .example-obtuse+p {
            margin: 10px 150px 2em 0;
            text-align: right;
            font-style: italic;
        }
        /* creates the larger triangle */
        
        .example-obtuse:before {
            content: "";
            position: absolute;
            bottom: -30px;
            right: 80px;
            border-width: 0 0 30px 50px;
            border-style: solid;
            border-color: transparent #f3961c;
            /* reduce the damage in FF3.0 */
            display: block;
            width: 0;
        }
        /* creates the smaller triangle */
        
        .example-obtuse:after {
            content: "";
            position: absolute;
            bottom: -30px;
            right: 110px;
            border-width: 0 0 30px 20px;
            border-style: solid;
            border-color: transparent #fff;
            /* reduce the damage in FF3.0 */
            display: block;
            width: 0;
        }

    </style>



    <body class="w3-sand w3-content" style="max-width:1600px">

        <div class="w3-bar w3-black">
            <a href="admin.php" class="w3-bar-item w3-button" style="width:20%;"><b>Admin     Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
            <a href="view_students.php" class="w3-bar-item w3-button" style="width:20%"><b>Student List  <i class="fa fa-address-card-o" style="font-size:22px"></i></b></a>
            <a href="view_candidates.php" class="w3-bar-item w3-button" style="width:20%"><b>Candidate List  <i class="fa fa-user-circle-o" style="font-size:22px"></i> </b></a>
            <a href="update_bridge.php" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username'];?> Logged In <i class="fa fa-user-secret" style="font-size:19px"></i></b></a>
            <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
        </div>

        <div>
            <img src="strathmore_university_logo.png" alt="STRATHMORE UNIVERSITY" class="w3-image" width="300px" ; height="200px" style="display: block;
    margin: auto;
    width: 40%;" ;>
            <h1 style="text-align:center"><b>USER FEEDBACK ON THIS SYSTEM</b></h1><br>
        </div>
        <?php 
    
    
    $sql = "SELECT * FROM feedback ORDER BY sent_time DESC";
    $result = mysqli_query($conn, $sql);
    if ($result){
        while($row = mysqli_fetch_array($result)){
            echo '<blockquote class="example-obtuse"><b>
        <p>"'.$row["message"].'"</p></b>
    </blockquote>
    <p>User: "<b>'.$row["students_studentID"].'</b>"<br> At "'.$row["sent_time"].'"</p>
';
        }
    }else{
        echo "No feedback to display ".mysqli_error($conn);
    }
     
    ?>

        <!--

    <blockquote class="example-obtuse">
        <p>It’s not what you look at that matters, it’s what you see.</p>
    </blockquote>
    <p>Henry David Thoreau<br> At 10:00 a.m.</p>
-->

        <footer class="w3-container w3-padding-32 w3-dark-grey">
            <div class="w3-row-padding">
                <div class="w3-third">
                    <h3>FROM OUR OWN...</h3>
                    <ul class="w3-ul w3-hoverable">
                        <li class="w3-padding-16">
                            <img src="images/roy.jpg" class="w3-left w3-margin-right" style="width:50px">
                            <span class="w3-large">― Roy Murwa Khasiani, a proud Stratizen ―</span><br>
                            <span>“We vote as one, for those who will keep us all in oneness, for the sake of achieving one vision, as one people.” 
</span>
                    </ul>
                </div>

                <div class="w3-third">
                    <h3>FAMOUS QUOTES</h3>
                    <ul class="w3-ul w3-hoverable">
                        <li class="w3-padding-16">
                            <img src="images/alincoln.jpg" class="w3-left w3-margin-right" style="width:50px">
                            <span class="w3-large">― Abraham Lincoln ―</span><br>
                            <span>“Elections belong to the people. It's their decision. If they decide to turn their back on the fire and burn their behinds, then they will just have to sit on their blisters.” 
</span>
                    </ul>
                </div>

                <div class="w3-third">
                    <h3>MORE QUOTES</h3>
                    <ul class="w3-ul w3-hoverable">
                        <li class="w3-padding-16">
                            <img src="images/larrysabato.jpg" class="w3-left w3-margin-right" style="width:50px">
                            <span class="w3-large">― Larry J. Sabato ―</span><br>
                            <span>“Every election is determined by the people who show up.” 
</span>
                        </li>
                    </ul>
                </div>


                <div class="w3-black w3-center w3-padding-24">Powered by <a href="http://www.strathmore.edu" title="STRATHMORE UNIVERSITY" target="_blank" class="w3-hover-opacity">STRATHMORE UNIVERSITY</a></div>

                <!-- End page content -->
            </div>
        </footer>

    </body>

    </html>
