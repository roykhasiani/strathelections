<?php
session_start();
include("connection.php");

// if (!$_SESSION['auth']){
//     header("location: login.php");
// }

 if(!isset($_SESSION['username'])){
     header("location: login.php");
 }
else if($_SESSION['role'] == '1' ){
    header( "Refresh:1; url=results.php" );
    echo "<div class='w3-panel w3-pale-red'>
    <h3><b>Admin Cannot Vote!!!</b></h3>
    <h4>Now taking you to The Results Section in a second</h4>
  </div>";
    
}
$sql = "SELECT vote_statusID FROM student_vote_status WHERE `studentID` = ". $_SESSION['username']."";
$result = mysqli_query($conn, $sql);
if ($result){
    $row = mysqli_fetch_assoc($result);
        if(  $row["vote_statusID"] == 2){
            header( "Refresh:1; url=results.php" );
            echo "<html><link rel='stylesheet' href='w3.css'style='max-width:600px; margin-left:auto; margin-right:auto;'><div class='w3-panel w3-pale-red'>
    <h3><b>You have already Voted!!!</b></h3>
    <h4>Now taking you to The Results Section in a second...</h4>
  </div></html>";
             
//            exit();
        }
}
if (isset($_POST["Submit"])){
    
$sql = "SELECT vote_statusID FROM student_vote_status WHERE `studentID` = ". $_SESSION['username']."";
$result = mysqli_query($conn, $sql);
if ($result){
    $row = mysqli_fetch_assoc($result);
        if(  $row["vote_statusID"] == 2){
            echo "<div class='w3-panel w3-pale-red '>
    <h3><b>You have already Voted!!!</b></h3>
    <h4>Now taking you to The Results Section in 3, 2, 1...</h4>
  </div>";
             header( "Refresh:3; url=results.php" );
        }else{
            $vote_chairperson = $_POST["chairperson"];
$vote_vice_chairperson = $_POST["vice_chairperson"];
$vote_public_relations = $_POST["public_relations"];
$vote_sports = $_POST["sports"];
$vote_academic = $_POST["academic"];
$vote_financial = $_POST["financial"];
$vote_secretary = $_POST["secretary"];

$sql = "UPDATE candidatevotes SET `votes` = `votes` + 1 WHERE candidates_candidateID = '$vote_chairperson';";

$sql.= "UPDATE candidatevotes SET `votes` = `votes` + 1 WHERE candidates_candidateID = '$vote_vice_chairperson';";

$sql.= "UPDATE candidatevotes SET `votes` = `votes` + 1 WHERE candidates_candidateID = '$vote_public_relations';";

$sql.= "UPDATE candidatevotes SET `votes` = `votes` + 1 WHERE candidates_candidateID = '$vote_sports';";

$sql.= "UPDATE candidatevotes SET `votes` = `votes` + 1 WHERE candidates_candidateID = '$vote_academic';";

$sql.= "UPDATE candidatevotes SET `votes` = `votes` + 1 WHERE candidates_candidateID = '$vote_financial';";

$sql.= "UPDATE candidatevotes SET `votes` = `votes` + 1 WHERE candidates_candidateID = '$vote_secretary';";

    
$sql.= "UPDATE `student_vote_status` SET `vote_statusID` = 2 WHERE `studentID` = ". $_SESSION['username']."";


$result = mysqli_multi_query($conn, $sql);
     
    if ($result) {
           header( "Refresh:3; url=results.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Success !!!</b></h3>
    <h4>Now taking you to The Results Section in 3... 2... 1...</h4>
  </div>";
     
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $sql . "<br>" . mysqli_error($conn)."<p></div>";
    }
            
        }
    }
}


?>


    <!--FORM FOR VOTING-->


    <!DOCTYPE html>
    <html>
    <title>VOTE</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">

    <link rel="stylesheet" href="w3.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
    <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="jquery.min.js"></script>
    <style>
        html,
        body,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "Raleway", sans-serif
        }

    </style>

    <body class="w3-light-grey">

        <div class="w3-bar w3-black">
            <a href="index.php" class="w3-bar-item w3-button" style="width:20%;"><b>Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
            <a href="results.php" class="w3-bar-item w3-button" style="width:20%"><b>Results <i class="fa fa-envelope-o" style="font-size:19px"></i></b></a>
            <a href="charts.php" class="w3-bar-item w3-button" style="width:20%"><b>Live Reports  <i class="fa fa-pie-chart" style="font-size:22px"></i> </b></a>
            <a href="update_bridge.php" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username']?> Logged In  <i class="fa fa-user-circle-o" style="font-size:22px"></i></b></a>
            <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
        </div>
        <div class="open">
            <div id="checktime" class="w3-card-4">
                <div class="w3-container w3-brown">
                    <h2>VOTE FOR THE CANDIDATE OF YOUR CHOICE

                    </h2>

                </div>
                <form class="w3-container" action="" method="post">

                    <!--test dropdown from db-->

                    <p>
                        <label class="w3-text-brown"><b>Chairperson Contestants</b></label>
                        <select class="w3-select w3-border w3-sand" name="chairperson" required>
                    <option value="" disabled selected>Select</option>
                     <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT candidateID, Fname, Lname, positions_positionID FROM candidates WHERE positions_positionID=1 ORDER BY Fname");   
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=".$row['candidateID'].">" .$row['Fname'] ." " .$row['Lname'] ."</option>" ;
}
?>    
                  </select>
                    </p>
                    <p>
                        <label class="w3-text-brown"><b>Vice Chairperson Contestants</b></label>
                        <select class="w3-select w3-border w3-sand" name="vice_chairperson" required>
                    <option value="" disabled selected>Select(Sorted in Ascending Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT candidateID, Fname, Lname, positions_positionID FROM candidates WHERE positions_positionID=2 ORDER BY Fname");   
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=".$row['candidateID'].">" .$row['Fname'] ." " .$row['Lname'] ."</option>" ;
}
?>    
                  </select>
                    </p>
                    <p>
                        <label class="w3-text-brown"><b>Public Relations Representative Contestants</b></label>
                        <select class="w3-select w3-border w3-sand" name="public_relations" required>
                    <option value="" disabled selected>Select(Sorted in Ascending Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT candidateID, Fname, Lname, positions_positionID FROM candidates WHERE positions_positionID=3 ORDER BY Fname");   
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=".$row['candidateID'].">" .$row['Fname'] ." " .$row['Lname'] ."</option>" ;
}
?>    
                  </select>
                    </p>
                    <p>
                        <label class="w3-text-brown"><b>Sports Representative</b></label>
                        <select class="w3-select w3-border w3-sand" name="sports" required>
                    <option value="" disabled selected>Select(Sorted in Ascending Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT candidateID, Fname, Lname, positions_positionID FROM candidates WHERE positions_positionID=4 ORDER BY Fname");   
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=".$row['candidateID'].">" .$row['Fname'] ." " .$row['Lname'] ."</option>" ;
}
?>    
                  </select>
                    </p>
                    <p>
                        <label class="w3-text-brown"><b>Academic Representative</b></label>
                        <select class="w3-select w3-border w3-sand" name="academic" required>
                    <option value="" disabled selected>Select(Sorted in Ascending Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT candidateID, Fname, Lname, positions_positionID FROM candidates WHERE positions_positionID=5 ORDER BY Fname");   
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=".$row['candidateID'].">" .$row['Fname'] ." " .$row['Lname'] ."</option>" ;
}
?>    
                  </select>
                    </p>
                    <p>
                        <label class="w3-text-brown"><b>Financial Representative</b></label>
                        <select class="w3-select w3-border w3-sand" name="financial" required>
                    <option value="" disabled selected>Select(Sorted in Ascending Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT candidateID, Fname, Lname, positions_positionID FROM candidates WHERE positions_positionID=6 ORDER BY Fname");   
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=".$row['candidateID'].">" .$row['Fname'] ." " .$row['Lname'] ."</option>" ;
}
?>    
                  </select>
                    </p>
                    <p>
                        <label class="w3-text-brown"><b>Secretary</b></label>
                        <select class="w3-select w3-border w3-sand" name="secretary" required>
                    <option value="" disabled selected>Select(Sorted in Ascending Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT candidateID, Fname, Lname, positions_positionID FROM candidates WHERE positions_positionID=7 ORDER BY Fname");   
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=".$row['candidateID'].">" .$row['Fname'] ." " .$row['Lname'] ."</option>" ;
}
?>    
                  </select>
                        <p>
                            <button class="w3-btn w3-brown" value="Submit" name="Submit" id="checktime">Send</button>
                        </p>
                </form>
            </div>
        </div>

        <!--        <div class="open">Shop is open</div>-->
        <div class="closed">
            <div class='w3-panel w3-pale-red' style='max-width:600px; height:100px; margin-left:auto; margin-right:auto; text-align:center;margin-top: 200px;'>
                <h4><b>Voting Has Been Closed!!!</b></h4>
                <h4><b>The voting period is active only from 6.00 a.m. to 9.00 p.m.</b></h4>
            </div>
        </div>

        <script type="text/javascript">
            var d = new Date();
            if (d.getHours() >= 6 && d.getHours() <= 21) {
                $(".open").show();
                $(".closed").hide();
            } else {
                $(".closed").show();
                $(".open").hide();
            }

        </script>


    </body>


    </html>
