<?php
session_start();
include("connection.php");
if (!$_SESSION['auth']){
    header("location: adminlogin.php");
}



if (isset( $_POST["submit_candidate"])){
$Fname = mysqli_real_escape_string($conn, $_POST["Fname"]);
$Lname = mysqli_real_escape_string($conn, $_POST["Lname"]);
    
//$image = addslashes(file_get_contents($_FILES['image']['tmp_name']));
    
$image = $_FILES['image']['tmp_name'];
$imgContent = addslashes(file_get_contents($image));
    
$studentID = mysqli_real_escape_string($conn, $_POST["studentID"]);
$positionID = mysqli_real_escape_string($conn, $_POST["positionID"]);

$sql = "INSERT INTO candidates (Fname, Lname, students_studentID, positions_positionID, image, has_manifesto)
VALUES ('$Fname', '$Lname', '$studentID', '$positionID',  '$imgContent', 1)";

if(mysqli_query($conn, $sql)){
$last_id = mysqli_insert_id($conn);
$sql2="INSERT INTO candidatevotes(votes, candidates_students_studentID, candidates_candidateID) VALUES ('0', '$studentID', '$last_id')";

$result = mysqli_query($conn, $sql2);
       if ($result) {
           header( "refresh:1; url=manageusers.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Data Inserted Successfully !!!</b></h3>
  </div>";
        
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $sql2 . "<br>" . mysqli_error($conn)."<p></div>";
    }
 
}
    
}

if(isset($_POST["submit_student"])){
    $studentID=mysqli_real_escape_string($conn, $_POST["studentID"]);
$password=mysqli_real_escape_string($conn, $_POST["password"]);
$hash= password_hash($password, PASSWORD_BCRYPT, array('cost'=>11));
$roleID=$_POST["option"];

//$passwordmd5=md5($password);
$sql = "INSERT INTO students (studentID, password, role_roleID)
VALUES ('$studentID', '$hash','$roleID');";
$sql.= "INSERT INTO student_vote_status(studentID, vote_statusID) VALUES ('$studentID', 1)";




$result = mysqli_multi_query($conn, $sql);
     if(($result !== true) and (mysqli_errno($conn) == 1062)) { 
        header( "refresh:1; url=manageusers.php" );
        echo "<div class='w3-panel w3-red'>
    <h3><b>Student is Already Registered !!!</b></h3>
  </div>";
     } 
    else if ($result) {
        header( "refresh:1; url=manageusers.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Data Inserted Successfully !!!</b></h3>
  </div>";
        
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $sql . "<br>" . mysqli_error($conn)."<p></div>";
    }
 
}

?>



    <!DOCTYPE html>
    <html>
    <title>MANAGE USERS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="w3mobile.css">
    <link rel="stylesheet" href="w3.css">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
    <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <style>
        html,
        body,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "Raleway", sans-serif
        }

    </style>

    <body>
        <div class="w3-bar w3-black">
            <a href="admin.php" class="w3-bar-item w3-button" style="width:20%;"><b>Admin     Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
            <a href="view_students.php" class="w3-bar-item w3-button" style="width:20%"><b>Student List  <i class="fa fa-address-card-o" style="font-size:22px"></i></b></a>
            <a href="view_candidates.php" class="w3-bar-item w3-button" style="width:20%"><b>Candidate List  <i class="fa fa-user-circle-o" style="font-size:22px"></i> </b></a>
            <a href="update_bridge.php" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username'];?> Logged In <i class="fa fa-user-secret" style="font-size:19px"></i></b></a>
            <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
        </div>

        <div class="w3-card-4">
            <div class="w3-container w3-brown">
                <h2>Add New Student</h2>
            </div>
            <form class="w3-container" action="" method="post">
                <p>
                    <label class="w3-text-brown"><b>Student ID</b></label>
                    <input class="w3-input w3-border w3-sand" name="studentID" type="number" pattern="[1-9]{5}" title="Student ID should only contain Integers from 1-9 " required></p>
                <p>
                    <label class="w3-text-brown"><b>Password</b></label>
                    <input class="w3-input w3-border w3-sand" name="password" type="password" pattern="[0-9]{5,}" title="Please insert 5 or more characters" required></p>
                <p>
                    <label class="w3-text-brown"><b>User Role</b></label>

                    <select class="w3-select w3-border w3-sand" name="option" required>
    <option value="" disabled selected>Select(Sorted in Alphabetical Order)</option>
     <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT roleID, roleName FROM role WHERE roleID>1 ORDER BY roleID"); 
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=". $row['roleID'] .">" .$row['roleName'] ."</option>" ;
}
?>    
  </select>
                </p>
                <p>
                    <button class="w3-button w3-brown" value="Submit" name="submit_student">Register Student</button></p>
            </form>
        </div>

        <div class="w3-card-4">
            <div class="w3-container w3-brown">
                <h2>Add New Candidate</h2>
            </div>
            <form class="w3-container" action="" method="post" enctype="multipart/form-data">
                <p>
                    <label class="w3-text-brown"><b>First Name</b></label>
                    <input class="w3-input w3-border w3-sand" name="Fname" type="text" pattern="[A-Za-z]{2,}" title="Please insert 2 or more letters" required></p>
                <p>
                    <label class="w3-text-brown"><b>Last Name</b></label>
                    <input class="w3-input w3-border w3-sand" name="Lname" type="text" pattern="[A-Za-z]{2,}" title="Please insert 2 or more letters" required></p>
                <p>
                    <label class="w3-text-brown"><b>Student ID</b></label>
                    <select class="w3-select w3-border w3-sand" name="studentID" required>
                    <option value="" disabled selected>Select(Sorted in Ascending Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT studentID,role_roleID FROM students WHERE role_roleID=2 ORDER BY studentID"); 
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=". $row['studentID'] .">" .$row['studentID'] ."</option>" ;
}
?>    
                  </select>
                </p>
                <p>
                    <label class="w3-text-brown"><b>Position</b></label>
                    <select class="w3-select w3-border w3-sand" name="positionID">
                    <option value="" disabled selected>Select(Sorted in Alphabetic Order)</option>
                    <?php
            include("connection.php");
            $sql = mysqli_query($conn, "SELECT positionID, positionName FROM positions ORDER BY positionName"); 
            $row = mysqli_num_rows($sql);
            while ($row = mysqli_fetch_array($sql)){ echo "
            <option value=". $row['positionID'] .">" .$row['positionName'] ."</option>" ;
}
?>   
                  </select>
                </p>
                <p>
                    <label class="w3-text-brown"><b>Candidate Image</b></label>
                    <input class="w3-input w3-border w3-sand" name="image" type="file" placeholder="Maximum image size is 64Kb" required></p>

                <p>
                    <button class="w3-button w3-brown" value="Submit" name="submit_candidate">Register Candidate</button></p>
            </form>
        </div>



    </body>

    </html>
