<?php  
session_start();
include("connection.php");
 ?>



<!DOCTYPE html>
<html>
<title>Candidates</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
<link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
<link rel="stylesheet" href="w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<script src="w3.js"></script>

<style>
    html,
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", sans-serif
    }
    
    .search {
        position: relative;
        color: #aaa;
        font-size: 16px;
    }
    
    .search input {
        text-indent: 32px;
    }
    
    .search .fa-search {
        position: absolute;
        top: 10px;
        left: 10px;
    }

</style>

<body class="w3-light-grey">
    <div class="w3-bar w3-black">
        <a href="index.php" class="w3-bar-item w3-button" style="width:20%;"><b>Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
        <a href="results.php" class="w3-bar-item w3-button" style="width:20%"><b>Results <i class="fa fa-paper-plane-o" style="font-size:19px"></i></b></a>
        <a href="admin.php" class="w3-bar-item w3-button" style="width:20%"><b>Admin Area  <i class="fa fa-user-secret" style="font-size:22px"></i></b></a>
        <a href="<?php if(isset($_SESSION['auth'])){ echo " update_bridge.php ";}else{ echo "login.php ";}?>" class="w3-bar-item w3-button" style="width:20%"><b><?php if(isset($_SESSION['auth'])){ echo $_SESSION['username']. " Logged In";}else{ echo "Log In To Vote";}?>  <i class="fa fa-sign-in" style="font-size:22px"></i> </b></a>

        <?php if(isset($_SESSION['auth'])){ echo "  <a href='logout.php' class='w3-bar-item w3-button' style='width:20%'><b>Log Out   <i class='fa fa-sign-out' style='font-size:22px'></i></b></a>";}else{ echo "<a href='candidate_profiles.php' class='w3-bar-item w3-button' style='width:20%'><b>Candidate Profiles  <i class='fa fa-user' style='font-size:24px'></i></b></a>";}?>


        <!--        <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>-->
    </div>

    <div class="w3-container w3-sand"><br>
        <div>
            <img src="strathmore_university_logo.png" alt="STRATHMORE UNIVERSITY" class="w3-image" width="300px" ; height="200px" style="display: block;
    margin: auto;
    width: 40%;" ;>
            <h1 style="text-align:center"><b>CANDIDATE LIST FOR THIS ELECTION</b></h1><br>
        </div>
        <div class="search">
            <span class="fa fa-search"></span>
            <input class="w3-input w3-border w3-padding" oninput="w3.filterHTML('#id01', '.item', this.value)" placeholder="Search for Candidate..">
        </div>
        <p></p>
        <h3 style="text-align:center"><b>Chairperson Contestants</b>
        </h3>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 1";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand w3-hoverable item">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:50px;height:45px"/>';?>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Fname"];?></b>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Lname"]; ?></b>
                </td>

            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3 style="text-align:center"><b>Vice Chairperson Contestants</b></h3>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 2 ORDER BY candidatevotes.votes  DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand item">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:50px;height:45px"/>';?>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Fname"];?></b>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Lname"]; ?></b>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>
        <h3 style="text-align:center"><b>Public Relations Representative Contestants</b></h3>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>

            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 3 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand item">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:50px;height:45px"/>';?>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Fname"];?></b>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Lname"]; ?></b>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>


        <h3 style="text-align:center"><b>Sports Representative Contestants</b></h3>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 4 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand item">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:50px;height:45px"/>';?>
                </td>
                <td class="w3-text-brown"><b>
                        <?php echo $row["Fname"];?></b>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Lname"]; ?></b>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3 style="text-align:center"><b>Academic Representative Contestants</b></h3>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 5 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand item">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:50px;height:45px"/>';?>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Fname"];?></b>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Lname"]; ?></b>
                </td>

            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3 style="text-align:center">
            <b>Financial Representative Contestants</b>
        </h3>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 6 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand item">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:50px;height:45px"/>';?>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Fname"];?></b>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Lname"]; ?></b>
                </td>
            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>

        <h3 style="text-align:center"><b>Secretary Contestants</b></h3>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand">
            <tr class="w3-brown">
                <th>Image</th>
                <th>First Name</th>
                <th>Last Name</th>
            </tr>
            <?php  
             $sql = "SELECT candidates.positions_positionID, candidates.image, candidates.Fname, candidates.Lname, candidatevotes.votes FROM candidates INNER JOIN candidatevotes ON candidates.candidateID = candidatevotes.candidates_candidateID WHERE candidates.positions_positionID = 7 ORDER BY candidatevotes.votes DESC";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          ?>
            <tr class="w3-sand item">
                <td>
                    <?php 
                                   $image = $row["image"];
                                   echo'<img src="data:image/jpeg;base64,'. base64_encode($image) .'" class="w3-bar-item w3-circle" style="width:50px;height:45px"/>';?>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Fname"];?></b>
                </td>
                <td class="w3-text-brown"><b>
                    <?php echo $row["Lname"]; ?></b>
                </td>

            </tr>

            <?php  
                               }  
                          }  
                          ?>
        </table>
    </div>

    <!-- Contact Section -->
    <!--<div class="w3-container w3-padding-large w3-grey">-->
    <!--  <h4 id="contact"><center><b>Contact Us</b></center></h4>-->
    <!--  <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">-->
    <!--    <div class="w3-third w3-dark-grey">-->
    <!--      <p><i class="fa fa-envelope w3-xxlarge w3-text-light-grey"></i></p>-->
    <!--      <p>strathelections@gmail.com</p>-->
    <!--    </div>-->
    <!--    <div class="w3-third w3-teal">-->
    <!--      <p><i class="fa fa-map-marker w3-xxlarge w3-text-light-grey"></i></p>-->
    <!--      <p>Nairobi, Kenya</p>-->
    <!--    </div>-->
    <!--    <div class="w3-third w3-dark-grey">-->
    <!--      <p><i class="fa fa-phone w3-xxlarge w3-text-light-grey"></i></p>-->
    <!--      <p>0790434029</p>-->
    <!--    </div>-->
    <!--  </div>-->
    <!--  <hr class="w3-opacity">-->
    <!--  <form action="" method="post">-->
    <!--    <div class="w3-section">-->
    <!--      <p>      -->
    <!--          <label ><b>Your Feedback On The System</b></label>-->
    <!--          <textarea class="w3-input w3-border" style="resize:none" name="feedback" required></textarea>-->
    <!--       </p>-->
    <!--    </div>-->
    <!--    <button type="submit" class="w3-button w3-black w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Send Message</button>-->
    <!--  </form>-->
    <!--</div>-->



    <footer class="w3-container w3-padding-32 w3-dark-grey">
        <div class="w3-row-padding">
            <div class="w3-third">
                <h3>FROM OUR OWN...</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/roy.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Roy Murwa Khasiani, a proud Stratizen ―</span><br>
                        <span>“We vote as one, for those who will keep us all in oneness, for the sake of achieving one vision, as one people.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>FAMOUS QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/alincoln.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Abraham Lincoln ―</span><br>
                        <span>“Elections belong to the people. It's their decision. If they decide to turn their back on the fire and burn their behinds, then they will just have to sit on their blisters.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>MORE QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/larrysabato.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Larry J. Sabato ―</span><br>
                        <span>“Every election is determined by the people who show up.” 
</span>
                    </li>
                </ul>
            </div>


            <div class="w3-black w3-center w3-padding-24">Powered by <a href="http://www.strathmore.edu" title="STRATHMORE UNIVERSITY" target="_blank" class="w3-hover-opacity">STRATHMORE UNIVERSITY</a></div>

            <!-- End page content -->
        </div>
    </footer>

</body>

</html>
