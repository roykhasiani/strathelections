<?php
session_start();
if (isset($_SESSION["username"])){
    header("location:vote.php");
}else{
session_destroy();
include("connection.php");

if (isset($_POST["Submit"]))
{
    
            $usrname = mysqli_real_escape_string($conn, $_POST["username"]);
            $password = mysqli_real_escape_string($conn, $_POST["password"]);
            //$hpssword = password_hash($pssword, PASSWORD_BCRYPT, array('cost'=>11));

            $sql = "SELECT * FROM students WHERE studentID='$usrname'";

                
            $result = mysqli_query($conn, $sql);
            //$row = mysqli_fetch_assoc($result);
            //$numrows = mysqli_num_rows($result);

            $db_password = '';
                if($result){
                session_start();
                while($row = mysqli_fetch_assoc($result)){
                    $_SESSION['auth'] = 'true';
                    $_SESSION['username'] = $_POST['username'];
                    $_SESSION['role'] = $row['role_roleID'];
                    $db_password = $row['password'];
                        }
                if (password_verify($password, $db_password)){
                header("location: vote.php");
            }
            else{
                echo "<div class='w3-panel w3-pale-red' style='max-width:600px; margin-left:auto; margin-right:auto;'>
                <h4><b>Incorrect Credentials, Please Try Again!!! ".mysqli_error($conn)." </b></h4>
            </div>";
            //    header("location: candidates.php");
            }
                
                
            
            }
}
}

?>




    <!DOCTYPE html>
    <html>
    <title>STRATHMORE UNIVERSITY ELECTION SYSTEM</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
    <link rel="stylesheet" href="w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <style>
        body,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: "Raleway", sans-serif
        }

    </style>

    <body class="w3-light-grey w3-content">


        <div class="w3-card-4 w3-animate-zoom" style="max-width:600px; margin-left:auto; margin-right:auto; margin-top:50px;">
            <div class="w3-center"><br>
                <img src="images/avatar-sign.png" alt="Log In To Vote" style="width:30%" class="w3-circle w3-margin-top">
            </div>

            <form method="post" class="w3-container" action="">
                <div class="w3-section">
                    <h4 style="text-align: center"><b>Log In Here To Members Area</b></h4>
                    <label><b>Student ID Number</b></label>
                    <input class="w3-input w3-border w3-margin-bottom" type="number" placeholder="Enter Your Student ID Number eg 94382" name="username" required>
                    <label><b>Password</b></label>
                    <input class="w3-input w3-border" type="password" placeholder="Enter Password" name="password" required>
                    <button class="w3-button w3-block w3-brown w3-section w3-padding" type="submit" name="Submit" value="Submit"><b>Login      <i class="fa fa-sign-in"  style="font-size:18px;"></i></b></button>
                    <a href="index.php" class="w3-bar-item w3-button w3-brown" style="width:100%"><b>Back Home       <i class="fa fa-home" style="font-size:18px;"></i> </b></a>
                    <!--                <input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Remember me-->
                </div>
            </form>
        </div>

    </body>

    </html>
