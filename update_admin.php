<?php  
session_start();
include("connection.php");



if (!isset($_SESSION['auth'])){
    header("location: login.php");
}



if (isset($_POST["update"])){
    $password = mysqli_real_escape_string($conn, $_POST["password"]);
    $hash= password_hash($password, PASSWORD_BCRYPT, array('cost'=>11));
    $username = mysqli_real_escape_string($conn, $_POST["username"]);
    $updateQuery = "UPDATE admin SET userName='$username', password='$hash' WHERE adminID =' $_POST[hidden]'";
    
$result =mysqli_query($conn, $updateQuery);
    
    if ($result) {
        session_destroy();
        header( "refresh:1; url=adminlogin.php" );
        echo "<div class='w3-panel w3-pale-green'>
    <h3><b>Data Updated Successfully !!!</b></h3>
  </div>";
        
        
    } else {
        echo "<div class='w3-panel w3-red'><p>Error: " . $updateQuery . "<br>" . mysqli_error($conn)."<p></div>";
    }
    
}

 ?>



<!DOCTYPE html>
<html>
<title>Update Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="w3.css">
<link rel="stylesheet" href="w3mobile.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<script src="w3.js"></script>

<style>
    html,
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", sans-serif
    }
    
    .search {
        position: relative;
        color: #aaa;
        font-size: 16px;
    }
    
    .search input {
        text-indent: 32px;
    }
    
    .search .fa-search {
        position: absolute;
        top: 10px;
        left: 10px;
    }

</style>

<body class="w3-light-grey">
    <div class="w3-bar w3-black">
        <a href="admin.php" class="w3-bar-item w3-button" style="width:20%;"><b>Admin Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
        <a href="manageusers.php" class="w3-bar-item w3-button" style="width:20%"><b>Add Users <i class="fa fa-user-plus" style="font-size:19px"></i></b></a>
        <a href="view_candidates.php" class="w3-bar-item w3-button" style="width:20%"><b>Candidate List  <i class="fa fa-user-circle-o" style="font-size:22px"></i> </b></a>
        <a href="#" class="w3-bar-item w3-button" style="width:20%"><b><?php echo $_SESSION['username'];?> Logged In  <i class="fa fa-user-secret" style="font-size:22px"></i> </b></a>
        <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>
    </div>

    <div class="w3-container w3-sand"><br>
        <h1 style="text-align:center"><b>YOUR DETAILS</b></h1><br>
        <!--
<div class="search">
    <span class="fa fa-search"></span>
    <input class="w3-input w3-border w3-padding" oninput="w3.filterHTML('#id01', '.item', this.value)" placeholder="Search for Student..">
</div>
<p></p>
-->
        <h4 style="text-align:center">(You have to log in again once you change your password)</h4>
        <table id="id01" class="w3-table-all w3-hoverable w3-sand w3-card-4">
            <tr class="w3-brown">
                <th>Your Username</th>
                <th>Your New Password</th>
                <th>Action</th>
                <th></th>
            </tr>
            <?php  
             $sql = "SELECT * FROM admin";  
 $result = mysqli_query($conn, $sql);  

if($result === FALSE) { 
    die(mysqli_error($conn)); 
}
                          if(mysqli_num_rows($result) > 0)  
                          {  
                               while($row = mysqli_fetch_array($result))  
                               {  
                          
                echo "<form action='update_admin.php' method='post'>";
       
                echo "<tr class='w3-sand w3-hoverable item'>";
                    echo"<td class='w3-text-brown'><b> "  ."<input type = text name=username value=" .$row["userName"].
                    " </b></td>";
                    echo "<td class='w3-text-brown'><b> " ."<input type = 'password' name='password' pattern='(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}' title='Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters' required> </b>
                    </td>";
                    echo "<td class='w3-text-brown'>" . "<input type=submit name=update value=update" . " </td>";
                    echo"<td> "  ."<input type = hidden name=hidden value=" .$row["adminID"]. " </td>";
                    echo "</tr>";
                    echo "</form>";
                                   
                                   
                            
// echo "</tr>";


                               }  
                          }  
                          ?>
        </table>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
    <footer class="w3-container w3-padding-32 w3-dark-grey">
        <div class="w3-row-padding">
            <div class="w3-third">
                <h3>FROM OUR OWN...</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/roy.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Roy Murwa Khasiani, a proud Stratizen ―</span><br>
                        <span>“We vote as one, for those who will keep us all in oneness, for the sake of achieving one vision, as one people.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>FAMOUS QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/alincoln.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Abraham Lincoln ―</span><br>
                        <span>“Elections belong to the people. It's their decision. If they decide to turn their back on the fire and burn their behinds, then they will just have to sit on their blisters.” 
</span>
                </ul>
            </div>

            <div class="w3-third">
                <h3>MORE QUOTES</h3>
                <ul class="w3-ul w3-hoverable">
                    <li class="w3-padding-16">
                        <img src="images/larrysabato.jpg" class="w3-left w3-margin-right" style="width:50px">
                        <span class="w3-large">― Larry J. Sabato ―</span><br>
                        <span>“Every election is determined by the people who show up.” 
</span>
                    </li>
                </ul>
            </div>


            <div class="w3-black w3-center w3-padding-24">Powered by <a href="http://www.strathmore.edu" title="STRATHMORE UNIVERSITY" target="_blank" class="w3-hover-opacity">STRATHMORE UNIVERSITY</a></div>

            <!-- End page content -->
        </div>
    </footer>




</body>

</html>
