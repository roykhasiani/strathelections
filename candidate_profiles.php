<!DOCTYPE html>
<html>
<title>Manifestos</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3.css">
<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
<link rel="icon" href="favicon.ico" sizes="65x65" type="image/png">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
<script src="w3.js"></script>

<style>
    html,
    body,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Raleway", sans-serif
    }
    
    .search {
        position: relative;
        color: #aaa;
        font-size: 16px;
    }
    
    .search input {
        text-indent: 32px;
    }
    
    .search .fa-search {
        position: absolute;
        top: 10px;
        left: 10px;
    }

</style>

<body class="w3-sand">

    <div class="w3-bar w3-black">
        <a href="index.php" class="w3-bar-item w3-button" style="width:20%;"><b>Home  <i class="fa fa-home" style="font-size:22px"></i></b></a>
        <a href="results.php" class="w3-bar-item w3-button" style="width:20%"><b>Results <i class="fa fa-paper-plane-o" style="font-size:19px"></i></b></a>
        <a href="admin.php" class="w3-bar-item w3-button" style="width:20%"><b>Admin Area  <i class="fa fa-user-secret" style="font-size:22px"></i></b></a>
        <a href="<?php if(isset($_SESSION['auth'])){ echo " update_bridge.php ";}else{ echo "login.php ";}?>" class="w3-bar-item w3-button" style="width:20%"><b><?php if(isset($_SESSION['auth'])){ echo $_SESSION['username']. " Logged In";}else{ echo "Log In To Vote";}?>  <i class="fa fa-sign-in" style="font-size:22px"></i> </b></a>

        <?php if(isset($_SESSION['auth'])){ echo "  <a href='logout.php' class='w3-bar-item w3-button' style='width:20%'><b>Log Out   <i class='fa fa-sign-out' style='font-size:22px'></i></b></a>";}else{ echo "<a href='candidate_profiles.php' class='w3-bar-item w3-button' style='width:20%'><b>Candidate Profiles  <i class='fa fa-user' style='font-size:24px'></i></b></a>";}?>


        <!--        <a href="logout.php" class="w3-bar-item w3-button" style="width:20%"><b>Log Out   <i class="fa fa-sign-out" style="font-size:22px"></i></b></a>-->
    </div>


    <div>
        <h1 style="text-align:center"><b>CANDIDATE PROFILES AND ASPIRATIONS</b></h1><br>
    </div>

    <div class="w3-container w3-brown">
        <h3>Chairperson Contestants</h3>
    </div>
    <?php
        include("connection.php");
         $sql = "SELECT image, positions_positionID, Fname, Lname, manifesto_Content FROM candidates WHERE candidates.positions_positionID = 1";  

         $result = mysqli_query($conn, $sql);

         if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
            if(mysqli_num_rows($result) > 0)  
                     {  
                         echo "<div class='w3-row-padding w3-margin-top'>";
                          while($row = mysqli_fetch_array($result))  
                                 {  

                $image = $row["image"];
                    echo "  <div class='w3-third w3-margin-bottom w3-light-grey'>
                            <div class='w3-card-2'>
                            <img src='data:image/jpeg;base64,". base64_encode($image) ."' style='width:100%;  height:350px;' class='w3-hover-sepia'>
                            <div class='w3-container'>
                            <h4>". $row["Fname"]." ". $row["Lname"]. "</h4><h5><center><b>MANIFESTO: </b></center></h5>". $row["manifesto_Content"] ."
                            </div>
                            </div>
                            </div>";
                                             }
                        echo "</div>";
                        }
                    ?>


        <div class="w3-container w3-brown">
            <h3>Vice Chairperson Contestants</h3>
        </div>
        <?php
        include("connection.php");
         $sql = "SELECT image, positions_positionID, Fname, Lname, manifesto_Content FROM candidates WHERE candidates.positions_positionID = 2";  

         $result = mysqli_query($conn, $sql);

         if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
            if(mysqli_num_rows($result) > 0)  
                     {  
                         echo "<div class='w3-row-padding w3-margin-top'>";
                          while($row = mysqli_fetch_array($result))  
                                 {  

                $image = $row["image"];
                    echo "  <div class='w3-third w3-margin-bottom w3-light-grey'>
                            <div class='w3-card-2'>
                            <img src='data:image/jpeg;base64,". base64_encode($image) ."' style='width:100%;  height:350px;' class='w3-hover-sepia'>
                            <div class='w3-container'>
                            <h4>". $row["Fname"]." ". $row["Lname"]. "</h4><h5><center><b>MANIFESTO: </b></center></h5>". $row["manifesto_Content"] ."
                            </div>
                            </div>
                            </div>";
                                             }
                        echo "</div>";
                        }
                    ?>
            <div class="w3-container w3-brown">
                <h3>Public Relations Representative Contestants</h3>
            </div>
            <?php
        include("connection.php");
         $sql = "SELECT image, positions_positionID, Fname, Lname, manifesto_Content FROM candidates WHERE candidates.positions_positionID = 3";  

         $result = mysqli_query($conn, $sql);

         if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
            if(mysqli_num_rows($result) > 0)  
                     {  
                         echo "<div class='w3-row-padding w3-margin-top'>";
                          while($row = mysqli_fetch_array($result))  
                                 {  
                $image = $row["image"];
                    echo "  <div class='w3-third w3-margin-bottom w3-light-grey'>
                            <div class='w3-card-2'>
                            <img src='data:image/jpeg;base64,". base64_encode($image) ."' style='width:100%;  height:350px;' class='w3-hover-sepia'>
                            <div class='w3-container'>
                            <h4>". $row["Fname"]." ". $row["Lname"]. "</h4><h5><center><b>MANIFESTO: </b></center></h5>". $row["manifesto_Content"] ."
                            </div>
                            </div>
                            </div>";
                                             }
                        echo "</div>";
                        }
                    ?>

                <div class="w3-container w3-brown">
                    <h3>Sports Representative Contestants</h3>
                </div>
                <?php
        include("connection.php");
         $sql = "SELECT image, positions_positionID, Fname, Lname, manifesto_Content FROM candidates WHERE candidates.positions_positionID = 4";  

         $result = mysqli_query($conn, $sql);

         if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
            if(mysqli_num_rows($result) > 0)  
                     {  
                         echo "<div class='w3-row-padding w3-margin-top'>";
                          while($row = mysqli_fetch_array($result))  
                                 {  

                $image = $row["image"];
                    echo "  <div class='w3-third w3-margin-bottom w3-light-grey'>
                            <div class='w3-card-2'>
                            <img src='data:image/jpeg;base64,". base64_encode($image) ."' style='width:100%;  height:350px;' class='w3-hover-sepia'>
                            <div class='w3-container'>
                            <h4>". $row["Fname"]." ". $row["Lname"]. "</h4><h5><center><b>MANIFESTO: </b></center></h5>". $row["manifesto_Content"] ."
                            </div>
                            </div>
                            </div>";
                                             }
                        echo "</div>";
                        }
                    ?>

                    <div class="w3-container w3-brown">
                        <h3>Academic Representative Contestants</h3>
                    </div>
                    <?php
        include("connection.php");
         $sql = "SELECT image, positions_positionID, Fname, Lname, manifesto_Content FROM candidates WHERE candidates.positions_positionID = 5";  

         $result = mysqli_query($conn, $sql);

         if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
            if(mysqli_num_rows($result) > 0)  
                     {  
                         echo "<div class='w3-row-padding w3-margin-top'>";
                          while($row = mysqli_fetch_array($result))  
                                 {  

                $image = $row["image"];
                    echo "  <div class='w3-third w3-margin-bottom w3-light-grey'>
                            <div class='w3-card-2'>
                            <img src='data:image/jpeg;base64,". base64_encode($image) ."' style='width:100%;  height:350px;' class='w3-hover-sepia'>
                            <div class='w3-container'>
                            <h4>". $row["Fname"]." ". $row["Lname"]. "</h4><h5><center><b>MANIFESTO: </b></center></h5>". $row["manifesto_Content"] ."
                            </div>
                            </div>
                            </div>";
                                             }
                        echo "</div>";
                        }
                    ?>

                        <div class="w3-container w3-brown">
                            <h3>Financial Representative Contestants</h3>
                        </div>
                        <?php
        include("connection.php");
         $sql = "SELECT image, positions_positionID, Fname, Lname, manifesto_Content FROM candidates WHERE candidates.positions_positionID = 6";  

         $result = mysqli_query($conn, $sql);

         if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
            if(mysqli_num_rows($result) > 0)  
                     {  
                         echo "<div class='w3-row-padding w3-margin-top'>";
                          while($row = mysqli_fetch_array($result))  
                                 {  

                $image = $row["image"];
                    echo "  <div class='w3-third w3-margin-bottom w3-light-grey'>
                            <div class='w3-card-2'>
                            <img src='data:image/jpeg;base64,". base64_encode($image) ."' style='width:100%;  height:350px;' class='w3-hover-sepia'>
                            <div class='w3-container'>
                            <h4>". $row["Fname"]." ". $row["Lname"]. "</h4><h5><center><b>MANIFESTO: </b></center></h5>". $row["manifesto_Content"] ."
                            </div>
                            </div>
                            </div>";
                                             }
                        echo "</div>";
                        }
                    ?>

                            <div class="w3-container w3-brown">
                                <h3>Secretary Contestants</h3>
                            </div>
                            <?php
        include("connection.php");
         $sql = "SELECT image, positions_positionID, Fname, Lname, manifesto_Content FROM candidates WHERE candidates.positions_positionID = 7";  

         $result = mysqli_query($conn, $sql);

         if($result === FALSE) { 
                            die(mysqli_error($conn)); 
                        }
            if(mysqli_num_rows($result) > 0)  
                     {  
                         echo "<div class='w3-row-padding w3-margin-top'>";
                          while($row = mysqli_fetch_array($result))  
                                 {  

                $image = $row["image"];
                    echo "  <div class='w3-third w3-margin-bottom w3-light-grey'>
                            <div class='w3-card-2'>
                            <img src='data:image/jpeg;base64,". base64_encode($image) ."' style='width:100%;  height:350px;' class='w3-hover-sepia'>
                            <div class='w3-container'>
                            <h4>". $row["Fname"]." ". $row["Lname"]. "</h4><h5><center><b>MANIFESTO: </b></center></h5>". $row["manifesto_Content"] ."
                            </div>
                            </div>
                            </div>";
                                             }
                        echo "</div>";
                        }
                    ?>


                                <footer class="w3-container w3-padding-32 w3-dark-grey">
                                    <div class="w3-row-padding">
                                        <div class="w3-third">
                                            <h3>FROM OUR OWN...</h3>
                                            <ul class="w3-ul w3-hoverable">
                                                <li class="w3-padding-16">
                                                    <img src="images/roy.jpg" class="w3-left w3-margin-right" style="width:50px">
                                                    <span class="w3-large">― Roy Murwa Khasiani, a proud Stratizen ―</span><br>
                                                    <span>“We vote as one, for those who will keep us all in oneness, for the sake of achieving one vision, as one people.” 
</span>
                                            </ul>
                                        </div>

                                        <div class="w3-third">
                                            <h3>FAMOUS QUOTES</h3>
                                            <ul class="w3-ul w3-hoverable">
                                                <li class="w3-padding-16">
                                                    <img src="images/alincoln.jpg" class="w3-left w3-margin-right" style="width:50px">
                                                    <span class="w3-large">― Abraham Lincoln ―</span><br>
                                                    <span>“Elections belong to the people. It's their decision. If they decide to turn their back on the fire and burn their behinds, then they will just have to sit on their blisters.” 
</span>
                                            </ul>
                                        </div>

                                        <div class="w3-third">
                                            <h3>MORE QUOTES</h3>
                                            <ul class="w3-ul w3-hoverable">
                                                <li class="w3-padding-16">
                                                    <img src="images/larrysabato.jpg" class="w3-left w3-margin-right" style="width:50px">
                                                    <span class="w3-large">― Larry J. Sabato ―</span><br>
                                                    <span>“Every election is determined by the people who show up.” 
</span>
                                                </li>
                                            </ul>
                                        </div>


                                        <div class="w3-black w3-center w3-padding-24">Powered by <a href="http://www.strathmore.edu" title="STRATHMORE UNIVERSITY" target="_blank" class="w3-hover-opacity">STRATHMORE UNIVERSITY</a></div>

                                        <!-- End page content -->
                                    </div>
                                </footer>
</body>

</html>
