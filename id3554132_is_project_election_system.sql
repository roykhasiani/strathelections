-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 24, 2017 at 06:45 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id3554132_is_project_election_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminID` int(11) NOT NULL,
  `userName` varchar(45) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `role_roleID` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `candidateID` int(11) NOT NULL,
  `Fname` text NOT NULL,
  `Lname` text NOT NULL,
  `image` longblob,
  `students_studentID` int(11) NOT NULL,
  `positions_positionID` int(11) NOT NULL,
  `manifesto_Content` varchar(50000) NOT NULL,
  `has_manifesto` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `candidatevotes`
--

CREATE TABLE `candidatevotes` (
  `votesID` int(11) NOT NULL,
  `votes` int(11) DEFAULT NULL,
  `candidates_students_studentID` int(11) NOT NULL,
  `candidates_candidateID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `messageID` int(11) NOT NULL,
  `students_studentID` int(11) NOT NULL,
  `message` varchar(350) COLLATE utf8_unicode_ci NOT NULL,
  `sent_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `positionID` int(11) NOT NULL,
  `positionName` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`positionID`, `positionName`) VALUES
(1, 'Chairperson'),
(2, 'Vice Chairperson'),
(3, 'Public Relations Representatve'),
(4, 'Sports Representative'),
(5, 'Academic Representative'),
(6, 'Financial Representative'),
(7, 'Secretary');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `roleID` int(11) NOT NULL,
  `roleName` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`roleID`, `roleName`) VALUES
(1, 'Admin'),
(2, 'Candidate'),
(3, 'StudentVoter');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `studentID` int(11) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `role_roleID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`studentID`, `password`, `role_roleID`) VALUES
(11000, '$2y$11$9HCq.zXvz.ScYM7K8Tpii.qM.thqqjP.RLqGbtMnaEum7jxr/qkkG', 2),
(12000, '$2y$11$6mk1s2WzUET1PvnQLNsk9e6wkvJ5pUcNzUtkpVKzBghNfQKNxVA2K', 2),
(13000, '$2y$11$Aw.NS10G7bZuXDqlpDW2weu/WFK3rHgXqjpjcQQD.qfWRzmN/gWeK', 2),
(14000, '$2y$11$RDxHvkoHUV9AoyTeWA2XFe4Oz4pgYU5GNTSCZKp1z.hQ/aB9UbVxu', 2),
(15000, '$2y$11$Ov8s2XbE/aC9AtbUWGrcvugk9NkfzUIEMzIh3D5A.FNc19adyzZFu', 2),
(16000, '$2y$11$vldNZXDLznE4dNAWgZZlJeoOkT8YIcOI7zRnV4UO45kPqHjwp8lmW', 2),
(17000, '$2y$11$xu5ZFt4dMCFZqAHEf0Pa0OUNirVIVaIWIjoXRaxNc15dg463SbxGK', 3),
(66667, '$2y$11$3KqjfmJT14dZ1nVoFFcAw..g4i/hZrmNzb1MKdzYriiAtvEoHPK1K', 2),
(87654, '$2y$11$QGuG4dkBYkLC5XO3psPtDu/LjM8WIBw429bo59kcacmKYkowlLUF.', 2),
(88834, '$2y$11$KZy0rofikDtOo8D1E0qk0eoye8UnDeaiQZd6EQzsYdgt1P5EENIeC', 2),
(90000, '$2y$11$2N0hltun1DoGQ3ByXU79LO4/3/uXK8y5FMNZJwH9GCcxtG8a.Ncxm', 3),
(90001, '$2y$11$FsZUb9j8ByT7w369x6KmnunPnpSvlVK1kG04q2Qon60YmaNUWPXgS', 2),
(90002, '$2y$11$fuQ8Fipqt.IeYnxo4EHkLOxxkIjb/epRpUurcQxHB5kFvpl1xH7Ae', 2),
(90003, '$2y$11$zojh2hPaGcx17HJLkhcDf.ps8ChvJeotMmKN7vWHnovyrtYlpk9pC', 3),
(90004, '$2y$11$azXJwt8yLfuHVTt5WF0YpOcd.WwIlw5zK.HGbjNQMjfOx/i/qClEy', 2),
(90005, '$2y$11$8tLFzbJMqhFyRjGs9ut6COqcW5RvcdSeVKgAX3Yg7OcGI.AEjUy5y', 2),
(90006, '$2y$11$SU92zRjaRnyRs4wBs/6ngu2qF.Pm6IzPxhp1virepzEUDsFgN/HHe', 2),
(90007, '$2y$11$jgL/nnUb4uuYMVpUo0IfzONX9KQDodLHfYSKbvbit.nW4rogfb1PS', 2),
(90008, '$2y$11$zJwRUgnGI.Sr8LhVRGTWNuE54RPALqLKjdP2nHlBphtUVVx1NsBNy', 2),
(90009, '$2y$11$Pz3JYqgGWdKikej3llymheiug8GtCpyj6t1kE/cX4IQWTymkQSMwq', 2),
(90010, '$2y$11$U.3m.X5m81U4.bLq2sv4FuL.ANjtM8171f6ziDi7L9bwxQ0ebS1Qq', 2),
(90011, '$2y$11$Num3xpKvw1Ohnn60XZIzleVts4NuNj5SPPPeoATOuur9y6Oc95nB2', 2),
(90012, '$2y$11$vcPUoL1VkV7c6F18PWog7.oxI1i7l72XmzsZWSLe1GLAco2oFdVw6', 2),
(90013, '$2y$11$uXuxhyvqPCw5co6j3aPsXet5/G85XS2gQna9vjzAuZtQvNbb1Qggu', 2),
(90014, '$2y$11$F7irqUi5u3sX94gUzmPvne4pSg7RZvME48wjo6z91/stWssJF7AK.', 2),
(90015, '$2y$11$bt2jZTbRJ.z6lUudWGvO9.hzitOy1n5fPJCbguz4gDjjqgqeAe/be', 2),
(90022, '$2y$11$s.oUUYPOESl/SkRj2IpieeRozaTDjtjfHW6FugtnYaC5Gtiiukx4u', 3),
(91112, '$2y$11$3OWSe9yPFBG8eHoWuERuuecRrGGMPxlKeQJYdQtm.dRel211abbwy', 3),
(91113, '$2y$11$AlIRHM8/85rjZHjfBz5rLuVsWIGWI6t9oLQYgjAafBjKITxJQrG3u', 3),
(91521, '$2y$11$VoAeaKqCrn3AtX6K5LElX.gbvoPFDJiHE/7VOTh0lDVRDWIaUrzBC', 3),
(91623, '$2y$11$eFtNuBFBqnLLSoX14iuCUuDICTfMoVAFscBmSW1Vv53xwC5vBao8m', 3),
(91746, '$2y$11$x34WiXm8W6EARetpnhWvQ.j2re5rDvEGjHHMh0t4CZzzF2AWIV9mO', 3),
(91777, '$2y$11$.eOCQz//6OSIEv/3ZflR4Or7eOMlFPKPhJnrRgUOYdlnweHPcGiIC', 3),
(91888, '$2y$11$AO6ZlOFEea02Czo7Eii8oetczfoB6XgyiwpFwn2LcRvKhY5trs//a', 2),
(91911, '$2y$11$FPHBfqU50gFixqXmNwCatOSrw8JUK6Zidq1vZYT0lgMAwAxIum1nq', 2),
(92196, '$2y$11$r4ZYGdiqP3q3l4izq98ibeUIRjNlpELntEtrN5BpNoO3vNlwoK2dO', 3),
(92411, '$2y$11$v8GcBQbQ5u7ESuHNPAw8L.PhlPAkQ4zpfceFQkNGG/1fPcku4pJTa', 3),
(92742, '$2y$11$5P25VwGNG5dF7uCS9ZzMiejRaAGtruHAOua8AZXXZ9XFNjHiQoY8u', 3),
(92922, '$2y$11$1ou6/lNn2yymSCimWZgM7ODucelVqFxlZwP7OiM5VQNAzNcfjeYKm', 2),
(92980, '$2y$11$BtL/bwuWhIXNIwb1EtIM4OBwzkpnjKTRIRCXp5cQ4Pn0GNlE5rJte', 3),
(93914, '$2y$11$0cusUfrnwHw817jcIFR9CO0ys2nOeb1L6n7w7XvvR9CMW2BraCd7m', 3),
(94182, '$2y$11$QsQs/iUHRH2n4dpiUWXbt.Axyz5UVfIirRtJBqdwwfYCp6o2aSmNG', 3),
(94223, '$2y$11$gX9ZE1SS7VfUhfY122tEN.WrW2Oq..ReNobftnx1FGdbFkoU7xsx6', 3),
(94382, '$2y$11$nHx/q.ZSzQkL1O/TqAL.HeyUdRhoqv7h8Y2mwLJ.FtJCMmdmq6KHO', 3),
(94456, '$2y$11$w.B1H03LEduis6RmYZJ9oekOJNAnRuaozMcFoEHH4Zm2DqwBrC.5.', 3),
(94458, '$2y$11$4aP3xN/JcV7AWkA7WZfDyu53rS4jyoqcatF/Qeqt87cMTBvqtvuda', 3),
(94489, '$2y$11$ja3LyXZVrgpXcjGXwtSxhOujIJc8JG2r50zyZ7/zFWP9AAVIZ6E76', 3),
(94490, '$2y$11$DFIuS2ekqbf.UWVtOrru8euihVBWwA2KcS.ttyAUVAE4eu1WNJ1Ii', 3),
(94492, '$2y$11$z0iloDLt/0IalIg5PHebJumuWKDiMg0rCd27X34ONM3Rw6M3zANhy', 3),
(94499, '$2y$11$d9TvwtDE2LDP4cYFPDJqNuoeGPnnLGTdXbE91TW0r4DtiGl356UC2', 3),
(94503, '$2y$11$zRBUxL2kvcEwZjw7xynSvuKRJAadwrLlsDNosFDbjLE71r0b01gjG', 3),
(94573, '$2y$11$YoommsNiP484ClXXa.P/ZOCDVtywwnKhWPOMXw30Fb58j02V8Lg8i', 3),
(94574, '$2y$11$5wdv/LXplAXgEKPEzaE5z.Qe2/2wGK2bmKNVmOD9/HfWLVZRixscu', 3),
(94590, '$2y$11$6YJGjZllfZAwtf4Pe1hfueMoi7/OZDsm3EB5QmH/fh2MYYxkcsNoS', 3),
(94620, '$2y$11$38jeNTH14S61zTI7r93f7OpHHM1dBFuiYquX0/W.mzzVc1lBA9msy', 3),
(94621, '$2y$11$KrrpRUxl.3EppGFXGimnCenqFlggN0nHTLYb6euIuset.q9Tc4v2a', 3),
(94656, '$2y$11$yZzSiZcOLN.zL3RYbRIITussQp6zaVW3nMnTozOQc4U7H7afK88ge', 3),
(94657, '$2y$11$nVWiL.iJrpCcinv4cBhe8uZ/eP2YEbTQSm/.0gyRQvtiymc5qgYOW', 3),
(94788, '$2y$11$n.vVyIrNQ9G3MjWpHN13leRcqny1tFObEY/MJ5MaUuLlABnm.Gbjq', 3),
(94811, '$2y$11$Gb0cA9QHfnM7mJtj4NChGOpYtZu.Oka2Dvy/uRiLuqW3B3Y3/06BC', 3),
(94822, '$2y$11$9QFihrtDI6Eazo3RIddrq.PMo.Y3p60W0zjcf09R0EiXOvaRIwdCm', 3),
(94871, '$2y$11$F0JKxXo4oQMMAGZQW9Fk7e9VYDOu1jUWy/B1ueeCTsi9YQ0EG8Jvi', 3),
(94881, '$2y$11$ersOekQOoLLjpma/OZlxZu6cFr5TEm4fqu9Tu/7rJHAYO7elkGvVS', 3),
(94946, '$2y$11$yLnAWFNIXUnl35EYWz.S1ePqa81WcR9Rr31f9ilhoVq1W9FSRl442', 3),
(94969, '$2y$11$dtPHl0FBIdpqpVVtZWFvge5HDQSrZOI9oEm0K7g/B0JdfC1uKlsxG', 3),
(95000, '$2y$11$sBgZwQpkM.GuV5V67Z3/PulJGA7S/oSKo//.j/rOVa8BUt43osl0W', 3),
(95002, '$2y$11$56hKzKWU6u25nUYSzX85muaciB.Pb83rut1S2EjiwkymLQgTThKTS', 3),
(95003, '$2y$11$P0g/FWad6pn1WuNFn9dt4ujPSf3gGd3W/.or5uHZMeOXwYb1vQNX6', 3),
(95106, '$2y$11$9BJ9AOB7JYwzgzPUu4WUQeUqnNBvzY0IEdXgVsJseu2jDAFnPnriu', 3),
(95122, '$2y$11$3xj.o370yCn2rNwu2161K.ZNmPipgZuTGBDoe6TRa4qiUpxAg5Bzq', 2),
(95157, '$2y$11$CvBSj5QZTaGXZ3kwkHuZEObrX1Xkzj1RnuUy5il2S3g2AuIIUyV7e', 3),
(95159, '$2y$11$ztVmXKrQV8KHgyKu9kQ3..pgpsg2XAbW7CQ.GasJETy3xIeWSRjqm', 3),
(95188, '$2y$11$DUewusIE2Ozf/Qx27m0ipebRGG8.S52ST3elLzT0ZlK4Jmg2ZeY/6', 3),
(95197, '$2y$11$DPmKFtfypAMnJQWAPWVQMuGF1xX73fTUu1LBEIpOx81oZaqPlduMq', 3),
(95199, '$2y$11$Rozi4ChZ.pUM7rqPgU35heIv32yFeg3hBjiABvoWQbwEs3Aq6u0Wq', 3),
(95240, '$2y$11$euPO.M7XCZVUdFa40OVjUuL2XhStW7JRZYDFDBdby0J54d9wEdFx6', 3),
(95303, '$2y$11$WWyIJCBgrOEasXYnYhfXjuXye5rQ8B7JfJNlqmHA9rnQgiJVLbq3q', 3),
(95313, '$2y$11$q6gYB/BB9ShjXKgBqdlASuh2vrzqsQ4RCp9BDEnP6x5TUvYQcSCfy', 3),
(95413, '$2y$11$cvRv0if0hnsjzaWzSaJESem/kpNLEM/d3WzSV9CNODfDTfwMrNr6y', 3),
(95507, '$2y$11$tuHMo.xi.WqwvjKZkYfZpOeq0GcWe1MyK6a6vQGTTOscQ0LXxgD7G', 3),
(95508, '$2y$11$ZtkHBuX6vjP8HOLzXne1LOy.NcfhPaD8j7e5WM2C3Wm8tRSrX/H66', 3),
(95555, '$2y$11$FInF/J6BUazRCG9SUkG1ru5dJ53Z0rp/r2FiTqP4Y1dRE0Oq7kG.y', 2),
(95655, '$2y$11$luJz3muSKesJvwAGcfI4Ie0PsoRvX7odRxuhmMAyDt2bIwq3ei9jO', 3),
(95815, '$2y$11$oMDn/8QVxnkLe1q8AYGl8OmWXX930M2jDYBUXBX8GIhveWHjWQM3y', 3),
(95816, '$2y$11$u0BMW.zmjE5PG6k.0MwTU.4w3tGsKYmjZhmTm6uXVy/2UqKIMNxwK', 3),
(95817, '$2y$11$GFaM0zFPeVLCT04Oq8dsNe.JTlIs4WdEY0jeA82MsZIfaefLpONkm', 3),
(95844, '$2y$11$YGaRlSOjxrKDQ2L83UspsOHznEthAe7g1rCNlxTTklCalqBJIkes6', 3),
(95900, '$2y$11$Kj2LEkosY25D1jxoDfFyo./pZaC1o17hSf0qtRCzWA3tReiOmvaS2', 3),
(96000, '$2y$11$6EVK0KtNX/WnejjkopAPR..ssNxXvQj96d8J35BPM0ZjF7D.QS/Qi', 2),
(96048, '$2y$11$gaaxJqX/0eaMJ.VowpuaQ.PH8KsCxx6YZ631hziTXr1APCbCxwfpK', 3),
(96130, '$2y$11$pHLh/S6CDmAb6UxXb/W6f.AzzmLKZ7CJcwCwuxYeGtdrX7/B9KVU6', 3),
(98765, '$2y$11$thp2/Nmw5gz08Y4EA8Snt.Jfil.1QzYhT2Y./QhuBE9qfcb9nmNz6', 2),
(98766, '$2y$11$01RoZW2tfeV4wVWxjHhJTOZaeqQ5kRjijUcq9E/t4UuhUH4DmkN8a', 2),
(99998, '$2y$11$3Idc.SK7UU28SVaNoPcb8eHgvmG9kamqyJ3R2jyYSaUmPw98w4ODq', 3),
(99999, '$2y$11$Xb8icwD/jZrYCAD6F7tLXuUGHx0T1Z.lPizIo9Enfv41WU10FCUXS', 2);

-- --------------------------------------------------------

--
-- Table structure for table `student_vote_status`
--

CREATE TABLE `student_vote_status` (
  `vte_statID` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `vote_statusID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_vote_status`
--

INSERT INTO `student_vote_status` (`vte_statID`, `studentID`, `vote_statusID`) VALUES
(1, 90000, 2),
(2, 90001, 1),
(3, 90002, 1),
(4, 90003, 1),
(5, 90022, 1),
(6, 90004, 1),
(7, 90005, 1),
(8, 90006, 1),
(9, 90007, 1),
(10, 90008, 1),
(11, 90009, 1),
(12, 90010, 1),
(13, 90011, 1),
(14, 90012, 1),
(15, 90013, 1),
(16, 90014, 1),
(17, 90015, 1),
(18, 94182, 1),
(19, 94382, 2),
(20, 95122, 1),
(21, 91112, 1),
(22, 91113, 1),
(23, 95000, 1),
(24, 96000, 1),
(25, 99999, 1),
(26, 98765, 1),
(27, 87654, 1),
(28, 98766, 1),
(29, 91911, 1),
(30, 66667, 1),
(31, 95555, 1),
(32, 94822, 1),
(33, 94811, 2),
(34, 95157, 1),
(35, 94788, 1),
(36, 91521, 2),
(37, 95106, 1),
(38, 94458, 1),
(39, 92980, 1),
(40, 95817, 1),
(41, 95159, 1),
(42, 95240, 1),
(43, 93914, 1),
(44, 94573, 1),
(45, 94223, 1),
(46, 94621, 1),
(47, 95003, 1),
(48, 94620, 1),
(49, 94489, 2),
(50, 95815, 2),
(51, 94499, 2),
(52, 95188, 1),
(53, 91623, 2),
(54, 95002, 1),
(55, 95199, 1),
(56, 94656, 1),
(57, 96048, 1),
(58, 95413, 1),
(59, 95844, 1),
(60, 94657, 1),
(61, 95507, 1),
(62, 94492, 2),
(63, 94871, 2),
(64, 94881, 1),
(65, 95197, 1),
(66, 94574, 1),
(67, 96130, 2),
(68, 94590, 1),
(69, 94503, 1),
(70, 95900, 2),
(71, 95313, 1),
(72, 91746, 1),
(73, 94946, 1),
(74, 92196, 1),
(75, 94456, 1),
(76, 95655, 1),
(77, 94490, 1),
(78, 95508, 1),
(79, 95303, 2),
(80, 92411, 1),
(81, 92742, 2),
(82, 94969, 1),
(83, 95816, 1),
(84, 99998, 1),
(85, 88834, 1),
(86, 91777, 1),
(87, 91888, 1),
(88, 11000, 1),
(89, 12000, 1),
(90, 13000, 1),
(91, 14000, 1),
(92, 15000, 1),
(93, 16000, 1),
(94, 17000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vote_status`
--

CREATE TABLE `vote_status` (
  `vote_statusID` int(11) NOT NULL,
  `vote_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vote_status`
--

INSERT INTO `vote_status` (`vote_statusID`, `vote_status`) VALUES
(1, 'Not Voted'),
(2, 'Has Voted');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminID`,`role_roleID`),
  ADD KEY `fk_admin_role1_idx` (`role_roleID`);

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`candidateID`,`students_studentID`,`positions_positionID`),
  ADD UNIQUE KEY `students_studentID` (`students_studentID`),
  ADD KEY `fk_candidates_students1_idx` (`students_studentID`),
  ADD KEY `fk_candidates_positions1_idx` (`positions_positionID`),
  ADD KEY `positions_positionID_2` (`positions_positionID`);

--
-- Indexes for table `candidatevotes`
--
ALTER TABLE `candidatevotes`
  ADD PRIMARY KEY (`votesID`),
  ADD UNIQUE KEY `candidates_students_studentID` (`candidates_students_studentID`),
  ADD UNIQUE KEY `candidates_candidateID` (`candidates_candidateID`),
  ADD KEY `fk_candidatevotes_candidates1_idx` (`candidates_students_studentID`),
  ADD KEY `candidate_candidateID` (`candidates_candidateID`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`messageID`),
  ADD KEY `studentID_FK_feedback` (`students_studentID`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`positionID`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`roleID`),
  ADD UNIQUE KEY `roleID` (`roleID`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`studentID`,`role_roleID`),
  ADD UNIQUE KEY `studentID` (`studentID`),
  ADD KEY `fk_students_role1_idx` (`role_roleID`);

--
-- Indexes for table `student_vote_status`
--
ALTER TABLE `student_vote_status`
  ADD PRIMARY KEY (`vte_statID`),
  ADD UNIQUE KEY `vte_statID` (`vte_statID`),
  ADD KEY `studentID` (`studentID`),
  ADD KEY `vote_statusID` (`vote_statusID`);

--
-- Indexes for table `vote_status`
--
ALTER TABLE `vote_status`
  ADD PRIMARY KEY (`vote_statusID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `candidateID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `candidatevotes`
--
ALTER TABLE `candidatevotes`
  MODIFY `votesID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `messageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `student_vote_status`
--
ALTER TABLE `student_vote_status`
  MODIFY `vte_statID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `vote_status`
--
ALTER TABLE `vote_status`
  MODIFY `vote_statusID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `fk_admin_role1` FOREIGN KEY (`role_roleID`) REFERENCES `role` (`roleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `candidates`
--
ALTER TABLE `candidates`
  ADD CONSTRAINT `fk_candidates_positions1` FOREIGN KEY (`positions_positionID`) REFERENCES `positions` (`positionID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_candidates_students1` FOREIGN KEY (`students_studentID`) REFERENCES `students` (`studentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `candidatevotes`
--
ALTER TABLE `candidatevotes`
  ADD CONSTRAINT `fk_candidates_candID` FOREIGN KEY (`candidates_candidateID`) REFERENCES `candidates` (`candidateID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_candidates_studID` FOREIGN KEY (`candidates_students_studentID`) REFERENCES `candidates` (`students_studentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `studentID_FK_feedback` FOREIGN KEY (`students_studentID`) REFERENCES `students` (`studentID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `fk_students_role1` FOREIGN KEY (`role_roleID`) REFERENCES `role` (`roleID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student_vote_status`
--
ALTER TABLE `student_vote_status`
  ADD CONSTRAINT `student_studentID_fk` FOREIGN KEY (`studentID`) REFERENCES `students` (`studentID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `student_vote_statusID_fk` FOREIGN KEY (`vote_statusID`) REFERENCES `vote_status` (`vote_statusID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
